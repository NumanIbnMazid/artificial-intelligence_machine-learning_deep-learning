import numpy as np


# In[2]:


incomes = np.random.normal(27000, 15000, 10000)


# ### Mean

# In[3]:


np.mean(incomes)


# In[4]:


import matplotlib.pyplot as plt
plt.hist(incomes, 50)
plt.show()


# ### Median

# In[5]:


np.median(incomes)


# ### Mode

# In[6]:


ages = np.random.randint(18, high=90, size=500)


# In[7]:


# ages


# In[9]:


from scipy import stats
stats.mode(ages)


# In[ ]:




