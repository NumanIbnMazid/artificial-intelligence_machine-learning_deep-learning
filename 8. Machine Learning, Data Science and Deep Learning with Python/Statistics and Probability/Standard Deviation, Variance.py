#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt


# In[2]:


incomes = np.random.normal(100.0, 20.0, 10000)


# In[3]:


incomes


# In[5]:


plt.hist(incomes, 50)
plt.show()


# ### Standard Deviation

# In[6]:


incomes.std()


# ### Variance

# In[7]:


incomes.var()


# In[ ]:




