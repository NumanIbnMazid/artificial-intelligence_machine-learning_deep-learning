# Mean:
- Average (Sum of values/num of values)
- 1, 2, 3 (6/3=2)

# Median:
- Middle Value (If even, Take average of two middle numbers)
- 1, 2, 3, 4, 5 (5)

# Mode
- Common values in data set
- 1, 1, 2, 3, 4 (1)

# Variance
- Variance (σ2) is the average of the squared differences from the mean
- What is the variance of data set (1, 4, 5, 4, 8)?
  - First find the mean (1+4+5+4+8) / 5 = 4.4
  - Now find the differences from the mean: (-3.4, -0.4, 0.6, -0.4, 3.6)
  - Find the squared differences: (11.56, 0.16, 0.36, 0.16, 12.96)
  - Find the average of the squared differences:
    - σ2 = (11.56+0.16+0.36+0.16+12.96) / 5 = 5.04

# Standard Deviation
- Standard Deviation σ is just the squared root of the variance
- σ2 = 5.04
- σ = √5.04 = 2.24
- So the Standard Deviation of (1, 4, 5, 4, 8) = 2.24
- Population Variance vs Sample Variance (See PDF)


