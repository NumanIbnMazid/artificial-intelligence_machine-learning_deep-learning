
# Variance

import numpy as np

data_set = (1,4,5,4,8)

data_mean = np.mean(data_set)

differences = []

for data in data_set:
    differences.append(round((data - data_mean),2))

differences = tuple(differences)

# print(differences)

squared_dif = []

for difference in differences:
    squared_dif.append(round((difference**2),2))

squared_dif = tuple(squared_dif)

variance = round((sum(squared_dif)/len(squared_dif)), 2)

print(variance)  # 5.04

# Sample variance N-1

sample_variance = round((sum(squared_dif) / (len(squared_dif)-1)), 2)

print(sample_variance) # 6.3


# Standard Deviation

# Standard Deviation is the square root of the variance
import math
standard_deviation = round(math.sqrt(variance), 2)

print(standard_deviation)  # 2.24

# In Numpy 

import numpy as np
import matplotlib.pyplot as plt

incomes = np.random.normal(100.0, 50, 10000)

print(incomes)

# plt.hist(incomes, 50)
# plt.show()

# Standard Deviation
print(incomes.std())
# Variance
print(incomes.var())

data_set = np.asarray(data_set)

# plt.hist(data_set)
# plt.show()

print(round(data_set.std(),2))
print(round(data_set.var(),2))
