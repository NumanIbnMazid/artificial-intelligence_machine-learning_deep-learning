import numpy as np
from matplotlib import pyplot as plt

values = np.random.uniform(-10.0, 10, 10000)

# plt.hist(values, 50)
# plt.show()

# Normal / Gaussian

from scipy.stats import norm, expon, binom, poisson
x = np.arange(-3, 3, 0.001)
# plt.plot(x, norm.pdf(x))
# plt.plot(x, expon.pdf(x))
# n, p = 10, 0.5
# x = np.arange(0, 10, 0.001)
# plt.plot(x, binom.pmf(x, n, p))
# mu = 500
# x = np.arange(400, 600, 0.5)
# plt.plot(x, poisson.pmf(x, mu))
# plt.show()
