import  numpy as np
import pandas as pd


# Pandas Data Frame
df = pd.read_csv("/home/numan/Dropbox/AI_LEARN/pandas/PastHires.csv")

print(df.head()) # First 5 Rows
print(df.head(10)) # First 10 Rows
print(df.tail(4)) # Last 4 rows

print(df.shape)  # (13, 7)
# 13 Rows, 7 Columns

print(df.size)  # 91
# Number of cells

print(len(df))  # 13
# Number of rows

print(df.columns)
# Index(['Years Experience', 'Employed?', 'Previous employers',
# 'Level of Education', 'Top-tier school', 'Interned', 'Hired'],
#     dtype = 'object')
# Actual Column Names


print(df['Hired'])
# Extracting specific column

print(df['Hired'][:5])
# First five rows of hired column

print(df['Hired'][5])
# Extract single row

print(df[['Years Experience', 'Hired']][:5])
# Extracting Multiple Columns

print(df.sort_values(['Years Experience']))
# Sorting values by columns


# Check How many unique Value exists
degree_counts = df['Level of Education'].value_counts()
print(degree_counts)
# BS     7
# PhD    4
# MS     2
# Name: Level of Education, dtype: int64

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# Generating Bar Chart
print(degree_counts.plot(kind='bar'))
# print(plt.plot([1,2,3]))

