
# Try extracting rows 5-10 of our DataFrame, preserving only the "Previous Employers" and "Hired" columns. Assign that to a new DataFrame, and create a histogram plotting the distribution of the previous employers in this subset of the data.


import os
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
WORKING_DIR = os.path.join(BASE_DIR, 'pandas')

df = pd.read_csv(f"{WORKING_DIR}/PastHires.csv")

exact_df = df[["Previous employers", "Hired"]][5:10]

degree_counts = df["Previous employers"].value_counts()

plot_bar = degree_counts.plot(kind='bar')

print(plot_bar)

