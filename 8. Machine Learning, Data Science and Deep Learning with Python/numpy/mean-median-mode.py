
import numpy as np
incomes = np.random.normal(27000, 15000, 10000)
mean = np.mean(incomes)

print(mean)

import matplotlib.pyplot as plt 
plt.hist(incomes, 50)
# plt.show()

median = np.median(incomes)
print(median)

incomes = np.append(incomes, [1000000000])

print(incomes)  


ages = np.random.randint(18, high=90, size=500)
print(ages)

from scipy import stats

mode = stats.mode(ages)
print(mode)  # ModeResult(mode=array([26]), count=array([13]))
