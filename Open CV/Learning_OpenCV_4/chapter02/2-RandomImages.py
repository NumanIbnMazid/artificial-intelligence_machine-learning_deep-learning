import cv2
import os
import numpy as np

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')
SAVE_PATH = f'{FILE_DIR}/converted'

# Make an array of 120,000 random bytes.
randomByteArray = bytearray(os.urandom(120000))
# print(randomByteArray)
flatNumpyArray = np.array(
    randomByteArray
)
print("\n Flat Numpy Array: \n", flatNumpyArray)

# Convert the array to make a 400x300 grayscale image.
grayImage = flatNumpyArray.reshape(300, 400)
print("\n Gray Image \n", grayImage)

cv2.imwrite(f"{SAVE_PATH}/RandomGray.png", grayImage)

# Convert the array to make a 400x100 color image.
bgrImage = flatNumpyArray.reshape(100, 400, 3)
print("BGR Image: \n", bgrImage)
cv2.imwrite(f'{SAVE_PATH}/RandomColor.png', bgrImage)
