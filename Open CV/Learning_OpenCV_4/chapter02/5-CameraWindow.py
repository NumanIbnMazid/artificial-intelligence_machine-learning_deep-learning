import cv2

clicked = False


def onMouse(event, x, y, flags, param):
    global clicked
    if event == cv2.EVENT_LBUTTONUP:
        clicked = True


cameraCapture = cv2.VideoCapture(0)
cv2.namedWindow("MyWindow")
cv2.setMouseCallback("MyWindow", onMouse)

print('Showing camera feed. Click window or press any key to stop.')

success, frame = cameraCapture.read()

"""
The argument for waitKey is a number of milliseconds to wait for keyboard input. By
default, it is 0, which is a special value meaning infinity.
The return value is either -1 (meaning that no key has been pressed) or an ASCII keycode, 
such as 27 for Esc.
Python provides a standard function, ord, which can convert a character into its ASCII keycode.
For example, ord('a') returns 97.
"""
# print(ord('a'))

while success and cv2.waitKey(1) == -1 and not clicked:
    cv2.imshow('MyWindow', frame)
    success, frame = cameraCapture.read()

cv2.destroyWindow('MyWindow')
cameraCapture.release()

"""
The mouse callback passed to setMouseCallback should take five arguments, as seen in
our code sample. The callback's param argument is set as an optional third argument to
setMouseCallback. By default, it is 0. The callback's event argument is one of the
following actions:

- cv2.EVENT_MOUSEMOVE: This event refers to mouse movement.
- cv2.EVENT_LBUTTONDOWN: This event refers to the left button going down when
it is pressed.
- cv2.EVENT_RBUTTONDOWN: This event refers to the right button going
down when it is pressed.
- cv2.EVENT_MBUTTONDOWN: This event refers to the middle button going
down when it is pressed.
- cv2.EVENT_LBUTTONUP: This event refers to the left button coming back up
when it is released.
- cv2.EVENT_RBUTTONUP: This event refers to the right button coming back
up when it is released.
- cv2.EVENT_MBUTTONUP: This event refers to the middle button coming back
up when it is released.
- cv2.EVENT_LBUTTONDBLCLK: This event refers to the left button being double clicked.
- cv2.EVENT_RBUTTONDBLCLK: This event refers to the right button being double clicked.
- cv2.EVENT_MBUTTONDBLCLK: This event refers to the middle button being double clicked.

The mouse callback's flags argument may be some bitwise combination of the following
events:

- cv2.EVENT_FLAG_LBUTTON: This event refers to the left button being pressed.
- cv2.EVENT_FLAG_RBUTTON: This event refers to the right button being pressed.
- cv2.EVENT_FLAG_MBUTTON: This event refers to the middle button being pressed.
- cv2.EVENT_FLAG_CTRLKEY: This event refers to the Ctrl key being pressed.
- cv2.EVENT_FLAG_SHIFTKEY: This event refers to the Shift key being pressed.
- cv2.EVENT_FLAG_ALTKEY: This event refers to the Alt key being pressed.
"""