import os
import cv2

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')
SAVE_PATH = f'{FILE_DIR}/converted'

# cv2.namedWindow("Numan's Image", cv2.WINDOW_NORMAL)

my_image = cv2.imread(f'{FILE_DIR}/nmn.jpg')
my_image_resized = cv2.resize(my_image, (500, 500))
cv2.imshow("Numan's Image", my_image_resized)

cv2.waitKey()
cv2.destroyAllWindows()