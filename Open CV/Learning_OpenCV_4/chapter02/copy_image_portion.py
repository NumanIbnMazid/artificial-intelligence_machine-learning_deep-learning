import cv2
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')
SAVE_PATH = f'{FILE_DIR}/converted'

"""
Defining regions of interests (ROI). Once the region is defined, we can perform a number of operations. For example, we can bind this region to a variable, define a second region, and assign the value of the first region to the second (hence, copying a portion of the image over to another position in the image)
"""
img = cv2.imread(f'{FILE_DIR}/MyPic.png')
print(img.shape)
# (290, 290, 3)
my_roi = img[0:290, 0:290]

my_image = cv2.imread(f'{FILE_DIR}/nmn.jpg')
print(my_image.shape)
# (1365, 1365, 3)
my_image[100:390, 100:390] = my_roi

cv2.imwrite(f"{SAVE_PATH}/CopiedImage.png", my_image)