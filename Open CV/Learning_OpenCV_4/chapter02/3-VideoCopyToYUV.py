import cv2
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')
SAVE_PATH = f'{FILE_DIR}/converted'

videoCapture = cv2.VideoCapture(f'{FILE_DIR}/MyInputVid.avi')
print("Video Capture: \n", videoCapture)

fps = videoCapture.get(cv2.CAP_PROP_FPS)
print("FPS: \n", fps)

size = (
    int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)),
    int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)),
)
print(size)

videoWriter = cv2.VideoWriter(
    f'{SAVE_PATH}/MyOutputVid.avi', cv2.VideoWriter_fourcc('I', '4', '2', '0'), fps, size
)
"""
- cv2.VideoWriter_fourcc('I','4','2','0'): This option is an
uncompressed YUV encoding, 4:2:0 chroma subsampled. This encoding is widely
compatible but produces large files. The file extension should be .avi.
- cv2.VideoWriter_fourcc('P','I','M','1'): This option is MPEG-1. The
file extension should be .avi.
- cv2.VideoWriter_fourcc('X','V','I','D'): This option is a relatively old
MPEG-4 encoding. It is a good option if you want to limit the size of the resulting
video. The file extension should be .avi.
- cv2.VideoWriter_fourcc('M','P','4','V'): This option is another
relatively old MPEG-4 encoding. It is a good option if you want to limit the size
of the resulting video. The file extension should be .mp4.
- cv2.VideoWriter_fourcc('X','2','6','4'): This option is a relatively
new MPEG-4 encoding. It may be the best option if you want to limit the size of
the resulting video. The file extension should be .mp4.
- cv2.VideoWriter_fourcc('T','H','E','O'): This option is Ogg Vorbis.
The file extension should be .ogv.
- cv2.VideoWriter_fourcc('F','L','V','1'): This option is a Flash video.
The file extension should be .flv.
"""

success, frame = videoCapture.read()

while success: # Loop until there are no more frames.
    videoWriter.write(frame)
    success, frame = videoCapture.read()