import numpy as np
import cv2

img = np.zeros((3, 3), dtype=np.uint8)

print("\n Gray Scale Image: \n", img)

"""
Here, each pixel is represented by a single 8-bit integer, which means that the values for each pixel are in the 0-255 range, where 0 is black, 255 is white, and the in-between values are shades of gray. This is a grayscale image.
"""

# Let's now convert this image into blue-green-red (BGR) format using the cv2.cvtColor function:

img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

print("\n blue-green-red (BGR) Image: \n", img)

"""
each pixel is now represented by a three-element array, with each integer representing one of the three color channels: B, G, and R, respectively.
"""

img = np.zeros((5, 3), dtype=np.uint8)
print("\n 5, 3 Image Numpy Shape: \n", img.shape)
"""
The preceding code will print (5, 3); in other words, we have a grayscale image with 5
rows and 3 columns. If you then converted the image into BGR, the shape would be (5,
3, 3), which indicates the presence of three channels per pixel.
"""

img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

print("\n blue-green-red (BGR) Image: \n", img)
print("\n blue-green-red (BGR) Image Shape: \n", img.shape)
