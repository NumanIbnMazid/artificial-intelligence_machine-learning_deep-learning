import cv2
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')
SAVE_PATH = f'{FILE_DIR}/converted'

cameraCapture = cv2.VideoCapture(0)
# 0 is the camera's device index
fps = 30
size = (int(cameraCapture.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(cameraCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)))

"""
To be more certain of the actual image dimensions, you
can first capture a frame and then get its height and width with code such as 
h, w = frame.shape[:2].
"""

videoWriter = cv2.VideoWriter(f"{SAVE_PATH}/10_sec_video_capture.avi",
                              cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, size)

success, frame = cameraCapture.read()

numFramesRemaining = 10 * fps - 1  # 10 seconds of frames

while numFramesRemaining > 0:
    if frame is not None:
        videoWriter.write(frame)
    success, frame = cameraCapture.read()
    numFramesRemaining -= 1