import cv2
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')

image = cv2.imread(f'{FILE_DIR}/MyPic.png')
# print(image)
SAVE_PATH = f'{FILE_DIR}/converted'
cv2.imwrite(os.path.join(SAVE_PATH, 'MyPic.jpg'), image)
cv2.waitKey(0)

"""
By default, imread returns an image in the BGR color format even if the file uses a
grayscale format. BGR represents the same color model as red-green-blue (RGB), but the
byte order is reversed.
"""
