import cv2
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')

my_image = cv2.imread(f"{FILE_DIR}/nmn.jpg")

print("Image Shape: ", my_image.shape)
# (1365, 1365, 3)
# (Height, Width, Num of Channels)
"""
For a grayscale image, we have len(shape) == 2, and for a color image, len(shape) == 3.
"""
print("Image Size: ", my_image.size)
# 5589675
# 1365 * 1365 * 3 = 5589675
"""
This is the number of elements in the array. In the case of a grayscale image, this is the same as the number of pixels. In the case of a BGR image, it is three times the number of pixels because each pixel is represented by three elements (B, G, and R).
"""
print("Image Dtype: ", my_image.dtype)
# uint8
"""
This is the datatype of the array's elements. For an 8-bit-per-channel
image, the datatype is numpy.uint8.
"""

