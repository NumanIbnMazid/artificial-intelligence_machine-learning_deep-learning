import cv2
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'chapter02')
SAVE_PATH = f'{FILE_DIR}/converted'

gray_image = cv2.imread(f'{FILE_DIR}/MyPic.png')
cv2.imwrite(f'{SAVE_PATH}/MyPicGray.png', gray_image)
