import cv2
import imutils
import os
from imutils.video import VideoStream

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.join(BASE_DIR, 'AIComputerVision')
VIDEO_DIR = os.path.join(PROJECT_DIR, 'video')
IMAGE_DIR = os.path.join(PROJECT_DIR, 'img')

# image = cv2.imread(f'{IMAGE_DIR}/input_image.jpg')
cap = cv2.VideoCapture(0)
# cap = cv2.VideoCapture(0)
# cap = VideoStream(src=0).start()

while True:
    ret, frame = cap.read()
    # frame = cap.read()
    frame = imutils.resize(frame, width=800)

    text = "This is my custom text"
    cv2.putText(frame, text, (5, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)

    cv2.rectangle(frame, (50, 50), (500, 500), (0, 0, 255), 2)

    cv2.imshow('Application', frame)

    key = cv2.waitKey(1)
    if key == ord('q'):
        break

cv2.destroyAllWindows()
