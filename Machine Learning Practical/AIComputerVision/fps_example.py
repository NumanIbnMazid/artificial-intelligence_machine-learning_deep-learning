import cv2
import datetime
import imutils
import os
from imutils.video import VideoStream

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.join(BASE_DIR, 'AIComputerVision')
VIDEO_DIR = os.path.join(PROJECT_DIR, 'video')
IMAGE_DIR = os.path.join(PROJECT_DIR, 'img')


def main():
    cap = cv2.VideoCapture(f'{VIDEO_DIR}/test_video.mp4')
    # cap = cv2.VideoCapture(0)
    # cap = VideoStream(src=0).start()

    fps_start_time = datetime.datetime.now()
    fps = 0
    total_frames = 0

    while True:
        ret, frame = cap.read()
        # frame = cap.read()
        frame = imutils.resize(frame, width=800)
        total_frames = total_frames + 1

        fps_end_time = datetime.datetime.now()
        time_diff = fps_end_time - fps_start_time
        if time_diff.seconds == 0:
            fps = 0.0
        else:
            fps = (total_frames / time_diff.seconds)

        fps_text = "FPS: {:.2f}".format(fps)

        cv2.putText(frame, fps_text, (5, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)

        cv2.imshow("Application", frame)
        key = cv2.waitKey(1)
        if key == ord('q'):
            break

    cv2.destroyAllWindows()


main()
