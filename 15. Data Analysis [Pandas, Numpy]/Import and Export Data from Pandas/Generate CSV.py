import pandas as pd
import datetime
import numpy as np


# In[2]:


df = pd.read_csv('salary_data.csv')


# In[5]:


df.dropna(axis=0, how='any', inplace=True)


# In[23]:


names = {"ANNUAL_RT":"Salary", "NAME":"Name", "JOBTITLE":"Job_Title", "HIRE_DT":"Hire_Date"}
df.rename(columns=names, inplace=True)


# In[26]:


df.drop(axis=1, labels=['DEPTID', 'DESCR', 'Gross'], inplace=True)


# In[28]:


df.info()


# In[42]:


df['Hire_Date'] = pd.to_datetime(df['Hire_Date'])
today = datetime.datetime.today()
df['Hire_Date'] = df['Hire_Date'].where(df['Hire_Date'] < today, df['Hire_Date'] -  np.timedelta64(100, 'Y'))


# In[43]:


df['Experience'] = (today - df['Hire_Date']).astype('<m8[Y]')


# In[45]:


df


# In[47]:


df.to_csv("employee_salary.csv", index=False)


# In[ ]:




