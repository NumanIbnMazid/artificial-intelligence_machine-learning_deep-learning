import pandas as pd
import os

# http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data

url = "http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data"

df = pd.read_csv(url, header=None)

data_dict = df.to_dict()

data_to_excel = df.to_excel('adult.xlsx')  # Save File

print(df.columns)
# Int64Index([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], dtype='int64')

df[[1, 2, 3]].to_excel('selected.xlsx')

print(pd.read_excel('adult.xlsx'))

df.to_html('adult.html')

df.to_json('adult.json')