import os
import pandas as pd
import matplotlib.pyplot as plt

# Customized Style
print(plt.style.available)
# ['Solarize_Light2', '_classic_test_patch', 'bmh', 'classic', 'dark_background', 'fast', 'fivethirtyeight', 'ggplot', 'grayscale', 'seaborn', 'seaborn-bright', 'seaborn-colorblind', 'seaborn-dark', 'seaborn-dark-palette', 'seaborn-darkgrid', 'seaborn-deep', 'seaborn-muted', 'seaborn-notebook', 'seaborn-paper', 'seaborn-pastel', 'seaborn-poster', 'seaborn-talk', 'seaborn-ticks', 'seaborn-white', 'seaborn-whitegrid', 'tableau-colorblind10']
plt.style.use('seaborn')
# %matplotlib inline  # (If in notebook)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(f"{FILE_DIR}/Stock.csv")

df.plot()
plt.show()

df['Open'].plot()
plt.show()

df[['Open', 'High']].plot()
plt.show()

df[['Open', 'High']].plot.line()
plt.show()