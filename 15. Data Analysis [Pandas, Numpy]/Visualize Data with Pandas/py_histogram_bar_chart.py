import os
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('seaborn')

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

print(df['sex'].unique())
# [' Male' ' Female']

counted = df['sex'].value_counts()
print(counted)
#  Male      21790
#  Female    10771
# Name: sex, dtype: int64

counted.plot(kind='bar')
plt.show()

counted.plot(kind='barh')  # Horizontal Bar Chart
plt.show()

df['education'].value_counts().plot(kind='barh')
plt.show()

df['sex'].value_counts().plot(kind='pie')
plt.show()

# Histogram for non-categorical data
df['age'].plot(kind='hist', bins=20)
plt.show()
