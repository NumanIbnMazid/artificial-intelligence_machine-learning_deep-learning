Date
# Completed - () :
# Next - () : 

30/03/2020
# Completed - (6. Numpy) : (4. Linear algebra with NumPy)
# Next - (6. Numpy) : (5. List vs NumPy Array)

02/04/2020
# Completed - (7. Series Pandas) : (3. Create Series from python object)
# Next - (7. Series Pandas) : (4. Create Series from CSV File)

03/04/2020
# Completed - (8. Data Frame Pandas) : (1. Introduction to Data Frame)
# Next - (8. Data Frame Pandas) : (2. Create Data Frame - random data + from File)

06/04/2020
# Completed - (8. Data Frame Pandas) : (16. .ix() method)
# Next - (8. Data Frame Pandas) : (17. .astype() method - optimize memory requirement)

07/04/2020
# Completed - (10. Panel Pandas) : (1. Warning - Panel Data type)
# Next - (11. Pandas Options) : (1. max_rows , max_columns)

08/04/2020
# Completed - (14. Working with Text Data) : (6. Processing on Column names)
# Next - (15. Data Grouping) : (1. Importing Data  Grouping)

09/04/2020
# Completed - (16. Data Frame  Multiindex) : (9. Pivot and Pivot_table Method)
# Next - (17. Working with Time series data) : (1. Python Date and Datetime module)

10/04/2020 (Shab-e-Barat)
# Completed - (18. Data cleaning) : (3. Data cleaning - Youtube Channel Dataset  Part - 3)
# Next - Completed

# ------------------ Completed ------------------