import pandas as pd

temperature = [30, 31, 32, 33, 34, 32, 30, 31, 29, 33, 34, 32]
months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

temp_series = pd.Series(data=temperature, index=months)

# Check length
print(len(temp_series))
# 12

# Check Type
print(type(temp_series))
# <class 'pandas.core.series.Series'>

# Convert to list
l_temp_series = list(temp_series)
print(l_temp_series)
# [30, 31, 32, 33, 34, 32, 30, 31, 29, 33, 34, 32]

# Convert to dictionary
d_temp_series = dict(temp_series)
print(d_temp_series)
# {'Jan': 30, 'Feb': 31, 'Mar': 32, 'Apr': 33, 'May': 34, 'Jun': 32, 'Jul': 30, 'Aug': 31, 'Sep': 29, 'Oct': 33, 'Nov': 34, 'Dec': 32}

# Sorting
print(sorted(temp_series))
# [29, 30, 30, 31, 31, 32, 32, 32, 33, 33, 34, 34]

# get maximum value
print(max(temp_series))
# 34

# Get minimum value
print(min(temp_series))
# 29
