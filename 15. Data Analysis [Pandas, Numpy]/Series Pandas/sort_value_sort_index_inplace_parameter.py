import pandas as pd

temperature = [30, 31, 32, 33, 34, 32, 30, 31, 29, 33, 34, 32]
months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

temp_series = pd.Series(data=temperature, index=months)

# Sort values
print(temp_series.sort_values())
# Sep    29
# Jan    30
# Jul    30
# Feb    31
# Aug    31
# Mar    32
# Jun    32
# Dec    32
# Apr    33
# Oct    33
# May    34
# Nov    34
# dtype: int64

print(temp_series.sort_values(ascending=False))
# Nov    34
# May    34
# Oct    33
# Apr    33
# Dec    32
# Jun    32
# Mar    32
# Aug    31
# Feb    31
# Jul    30
# Jan    30
# Sep    29
# dtype: int64

# affect main object / Permanent Modification
temp_series.sort_values(inplace=True)

print(temp_series)
# Sep    29
# Jan    30
# Jul    30
# Feb    31
# Aug    31
# Mar    32
# Jun    32
# Dec    32
# Apr    33
# Oct    33
# May    34
# Nov    34
# dtype: int64

# Sort index
print(temp_series.sort_index())
# Apr    33
# Aug    31
# Dec    32
# Feb    31
# Jan    30
# Jul    30
# Jun    32
# Mar    32
# May    34
# Nov    34p
# Oct    33
# Sep    29
# dtype: int64