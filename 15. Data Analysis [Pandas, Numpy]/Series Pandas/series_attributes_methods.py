import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'RESOURCES')

FILE = f'{FILE_DIR}/Stock.csv'

dfs = pd.read_csv(FILE, squeeze=True, usecols=['Open'])

# print(dfs)

# Attributes
# Check Indexing Mechanism
print(dfs.index)
# RangeIndex(start=0, stop=20, step=1)

# check values
print(dfs.values)
# [16.77     17.1      17.059999 16.9      16.969999 16.24     16.389999
#  16.549999 15.99     15.77     16.389999 16.33     16.34     16.24
#  16.469999 16.790001 17.1      17.299999 16.299999 16.459999]

# check data type
print(dfs.dtype)
# float64

# check dimension
print(dfs.ndim)
# 1

# check size
print(dfs.size)
# 20

# check if unique series
print(dfs.is_unique)
# False

# Adding Prefix
p_dfs = dfs.add_prefix('Panda')
print(p_dfs)
# Panda0     16.770000
# Panda1     17.100000
# Panda2     17.059999
# Panda3     16.900000
# Panda4     16.969999
# Panda5     16.240000
# Panda6     16.389999
# Panda7     16.549999
# Panda8     15.990000
# Panda9     15.770000
# Panda10    16.389999
# Panda11    16.330000
# Panda12    16.340000
# Panda13    16.240000
# Panda14    16.469999
# Panda15    16.790001
# Panda16    17.100000
# Panda17    17.299999
# Panda18    16.299999
# Panda19    16.459999
# Name: Open, dtype: float64


s_dfs = dfs.add_suffix("Panda")
print(s_dfs)
# 0Panda     16.770000
# 1Panda     17.100000
# 2Panda     17.059999
# 3Panda     16.900000
# 4Panda     16.969999
# 5Panda     16.240000
# 6Panda     16.389999
# 7Panda     16.549999
# 8Panda     15.990000
# 9Panda     15.770000
# 10Panda    16.389999
# 11Panda    16.330000
# 12Panda    16.340000
# 13Panda    16.240000
# 14Panda    16.469999
# 15Panda    16.790001
# 16Panda    17.100000
# 17Panda    17.299999
# 18Panda    16.299999
# 19Panda    16.459999
# Name: Open, dtype: float64

# Sum of series object
sum_dfs = dfs.sum()
print(sum_dfs)
# 331.459992

# check which index position maximum value occurs
print(dfs.idxmax())
# 17

# Get max value
print(max(dfs))
# 17.299999

# check which index position minimum value occurs
print(dfs.idxmin())
# 9

# Get Min Value
print(min(dfs))
# 15.77

# Get only some values (Default first Five Values)
dfs_head = dfs.head()
print(dfs_head)
# 0    16.770000
# 1    17.100000
# 2    17.059999
# 3    16.900000
# 4    16.969999
# Name: Open, dtype: float64

# Get Last values (Default last five values)
dfs_tail = dfs.tail()
print(dfs_tail)
# 15    16.790001
# 16    17.100000
# 17    17.299999
# 18    16.299999
# 19    16.459999
# Name: Open, dtype: float64


# Get First 10 values
dfs_f_10 = dfs.head(10)
print(dfs_f_10)

# Get last 10 values
dfs_l_10 = dfs.tail(10)
print(dfs_l_10)

# Get Average value
min_dfs = dfs.mean()
print(min_dfs)
# 16.5729996


# Multiply every value
print(dfs.product())
# 2.4295555370482454e+24

# Get standard diviation
std_dfs = dfs.std()
print(std_dfs)
# 0.4076130110008758

# Calculate every single statistics
des_dfs = dfs.describe()
print(des_dfs)
# count    20.000000
# mean     16.573000
# std       0.407613
# min      15.770000
# 25%      16.322500
# 50%      16.464999
# 75%      16.917500
# max      17.299999
# Name: Open, dtype: float64

# How many unique values
print(dfs.nunique())
# 17

# value counts
print(dfs.value_counts())
# 17.100000    2
# 16.240000    2
# 16.389999    2
# 16.969999    1
# 15.990000    1
# 16.459999    1
# 16.549999    1
# 16.330000    1
# 16.770000    1
# 16.299999    1
# 16.469999    1
# 16.340000    1
# 16.790001    1
# 15.770000    1
# 16.900000    1
# 17.299999    1
# 17.059999    1
# Name: Open, dtype: int64

