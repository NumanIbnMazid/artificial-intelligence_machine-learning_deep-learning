import pandas as pd

temperature = [30, 31, 32, 33, 34, 32, 30, 31, 29, 33, 34, 32]
months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

temp_series = pd.Series(data=temperature, index=months)

# Celsius to Fahrenheit
# Formula = C * 1.8 + 32


def convert_temp(C):
    return (C * 1.8) + 32


temp_series_f = temp_series.apply(convert_temp)


print(temp_series_f)

# Jan    86.0
# Feb    87.8
# Mar    89.6
# Apr    91.4
# May    93.2
# Jun    89.6
# Jul    86.0
# Aug    87.8
# Sep    84.2
# Oct    91.4
# Nov    93.2
# Dec    89.6
# dtype: float64


# Map Function

map_obj = {
    30: 300,
    31: 310,
    32: 320
}

mapped_obj = temp_series.map(map_obj)
print(mapped_obj)
# Jan    300.0
# Feb    310.0
# Mar    320.0
# Apr      NaN
# May      NaN
# Jun    320.0
# Jul    300.0
# Aug    310.0
# Sep      NaN
# Oct      NaN
# Nov      NaN
# Dec    320.0
# dtype: float64