# series = One diumensional labled array

import pandas as pd

# Create series
lst = [2, 45, 12, 23]
p_s = pd.Series(lst)
print(p_s)
# 0     2
# 1    45
# 2    12
# 3    23
# dtype: int64

lst_bool = [True, True, False, True]
p_s_b = pd.Series(lst_bool)
print(p_s_b)
# 0     True
# 1     True
# 2    False
# 3     True
# dtype: bool

lst_mxd = [23, "String", False, 24.4]
p_s_m = pd.Series(lst_mxd)
print(p_s_m)
# 0        23
# 1    String
# 2     False
# 3      24.4
# dtype: object


x = pd.Series(5, index=[0])
print(x)
# 0    5
# dtype: int64

y = pd.Series(5, index=[0, 1, 2, 3, 4])
print(y)
# 0    5
# 1    5
# 2    5
# 3    5
# 4    5
# dtype: int64


z = pd.Series([n*10 for n in range(10)])
print(z)
# 0     0
# 1    10
# 2    20
# 3    30
# 4    40
# 5    50
# 6    60
# 7    70
# 8    80
# 9    90
# dtype: int64


myphonebook = {
    "Abir": "01679987878",
    "Nishan": "01344545223",
    "Kabir": "01989944775"
}

d = pd.Series(myphonebook)
print(d)
# Abir      01679987878
# Nishan    01344545223
# Kabir     01989944775
# dtype: object
