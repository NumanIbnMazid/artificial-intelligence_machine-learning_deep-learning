import pandas as pd

temperature = [30, 31, 32, 33, 34, 32, 30, 31, 29, 33, 34, 32]
months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

temp_series = pd.Series(data=temperature, index=months)

print(temp_series[0])
# 30

print(temp_series[2:5])
# Mar    32
# Apr    33
# May    34
# dtype: int64

print(temp_series[[3, 5, 9]])
# Apr    33
# Jun    32
# Oct    33
# dtype: int64

print(temp_series['Aug'])
# 31

print(temp_series[[t > 32 for t in temp_series]])
# Apr    33
# May    34
# Oct    33
# Nov    34
# dtype: int64

