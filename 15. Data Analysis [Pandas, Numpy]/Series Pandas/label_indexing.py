import pandas as pd
import os

temperature = [30, 31, 32, 33, 34, 32, 30, 31, 29, 33, 34, 32]
months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

temp_series = pd.Series(data=temperature, index=months)

print(temp_series)
# Jan    30
# Feb    31
# Mar    32
# Apr    33
# May    34
# Jun    32
# Jul    30
# Aug    31
# Sep    29
# Oct    33
# Nov    34
# Dec    32
# dtype: int64

# Get indexes

print(temp_series.index)
# Index(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct',
#        'Nov', 'Dec'],
#       dtype='object')

