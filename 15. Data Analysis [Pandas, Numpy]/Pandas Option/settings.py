import pandas as pd
import numpy as np
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)
# https://pandas.pydata.org/pandas-docs/stable/options.html
default_max_columns = pd.options.display.max_columns
print(default_max_columns)
# 0
default_max_rows = pd.options.display.max_rows
print(default_max_rows)
# 60

pd.options.display.max_columns = 30
pd.options.display.max_rows = 20

print(pd.get_option('max_rows'))
# 20
print(pd.get_option('max_columns'))
# 30

pd.reset_option('max_rows')

print(pd.get_option('max_rows'))
# 60

pd.set_option('max_rows', 6)

print(pd.get_option('max_rows'))
# 6

print(pd.describe_option('max_rows'))
# display.max_rows : int
#     If max_rows is exceeded, switch to truncate view. Depending on
#     `large_repr`, objects are either centrally truncated or printed as
#     a summary view. 'None' value means unlimited.
#
#     In case python/IPython is running in a terminal and `large_repr`
#     equals 'truncate' this can be set to 0 and pandas will auto-detect
#     the height of the terminal and print a truncated object which fits
#     the screen height. The IPython notebook, IPython qtconsole, or
#     IDLE do not run in a terminal and hence it is not possible to do
#     correct auto-detection.
#     [default: 60] [currently: 6]


# Precision

precision = pd.options.display.precision  # Value after decimal
print(precision)
# 6

np_df = pd.DataFrame(np.random.randn(5, 5))
print(np_df)
#           0         1         2         3         4
# 0 -0.589455  0.545796 -0.060545 -0.491548 -0.953411
# 1  0.206899 -1.168678 -1.124832  0.349234 -0.132734
# 2  0.861433 -1.102042 -0.869227  1.014978 -1.367325
# 3 -0.795937  1.884210 -0.855265 -0.217957 -0.591849
# 4 -0.699755  0.450251  2.408831  0.278470  0.839515

pd.options.display.precision = 3

print(np_df)
#        0      1      2      3      4
# 0  0.728 -0.607  0.481 -0.746 -1.423
# 1  0.070  0.899 -0.725  0.457  0.540
# 2 -2.059 -2.087 -1.316 -0.016 -1.139
# 3  1.493  0.174 -0.388  0.392 -0.386
# 4 -0.251 -1.079 -0.466  0.792 -0.033

