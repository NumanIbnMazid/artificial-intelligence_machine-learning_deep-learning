import os
import pandas as pd
import numpy as np

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

print(df['sex'].apply(len)) # String has an extra space before
# 0        5
# 1        5
# 2        5
# 3        5
# 4        7
#         ..
# 32556    7
# 32557    5
# 32558    7
# 32559    5
# 32560    7
# Name: sex, Length: 32561, dtype: int64
con = df['sex'] == ' Male'
print(con)
# 0         True
# 1         True
# 2         True
# 3         True
# 4        False
#          ...
# 32556    False
# 32557     True
# 32558    False
# 32559     True
# 32560    False
# Name: sex, Length: 32561, dtype: bool

print(df[con]) # All male value containing data will return

#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 0       39          State-gov   77516  ...             40   United-States   <=50K
# 1       50   Self-emp-not-inc   83311  ...             13   United-States   <=50K
# 2       38            Private  215646  ...             40   United-States   <=50K
# 3       53            Private  234721  ...             40   United-States   <=50K
# 7       52   Self-emp-not-inc  209642  ...             45   United-States    >50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 32553   32            Private  116138  ...             11          Taiwan   <=50K
# 32554   53            Private  321865  ...             40   United-States    >50K
# 32555   22            Private  310152  ...             40   United-States   <=50K
# 32557   40            Private  154374  ...             40   United-States    >50K
# 32559   22            Private  201490  ...             20   United-States   <=50K
#
# [21790 rows x 15 columns]


print(df[df['age'] > 50])
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 3       53            Private  234721  ...             40   United-States   <=50K
# 7       52   Self-emp-not-inc  209642  ...             45   United-States    >50K
# 21      54            Private  302146  ...             20   United-States   <=50K
# 24      59            Private  109015  ...             40   United-States   <=50K
# 25      56          Local-gov  216851  ...             40   United-States    >50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 32542   72                  ?  129912  ...             25   United-States   <=50K
# 32548   65   Self-emp-not-inc   99359  ...             60   United-States   <=50K
# 32554   53            Private  321865  ...             40   United-States    >50K
# 32558   58            Private  151910  ...             40   United-States   <=50K
# 32560   52       Self-emp-inc  287927  ...             40   United-States    >50K
#
# [6460 rows x 15 columns]


# Filter with multiple condition

con1 = df['sex'] == " Male"
con2 = df['age'] > 50

print(con1.head())
# 0     True
# 1     True
# 2     True
# 3     True
# 4    False
# Name: sex, dtype: bool

print(con2.head())
# 0    False
# 1    False
# 2    False
# 3     True
# 4    False
# Name: age, dtype: bool

print((con1 & con2).head())
# 0    False
# 1    False
# 2    False
# 3     True
# 4    False
# dtype: bool

print(df[con1 & con2])  # print(df[con1 | con2])
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 3       53            Private  234721  ...             40   United-States   <=50K
# 7       52   Self-emp-not-inc  209642  ...             45   United-States    >50K
# 25      56          Local-gov  216851  ...             40   United-States    >50K
# 27      54                  ?  180211  ...             60           South    >50K
# 41      53   Self-emp-not-inc   88506  ...             40   United-States   <=50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 32533   54            Private  337992  ...             50           Japan    >50K
# 32539   71                  ?  287372  ...             10   United-States    >50K
# 32542   72                  ?  129912  ...             25   United-States   <=50K
# 32548   65   Self-emp-not-inc   99359  ...             60   United-States   <=50K
# 32554   53            Private  321865  ...             40   United-States    >50K
#
# [4568 rows x 15 columns]
