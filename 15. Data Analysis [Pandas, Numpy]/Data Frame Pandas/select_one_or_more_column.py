import os
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

df["Kind of Species"] = "Human Being"

print(df.head(2))
#    age          workclass  fnlwgt  ...  native-country  salary Kind of Species
# 0   39          State-gov   77516  ...   United-States   <=50K     Human Being
# 1   50   Self-emp-not-inc   83311  ...   United-States   <=50K     Human Being

df["Age in Month"] = df["age"] * 12

print(df.head(2))
#    age          workclass  fnlwgt  ...  salary  Kind of Species Age in Month
# 0   39          State-gov   77516  ...   <=50K      Human Being          468
# 1   50   Self-emp-not-inc   83311  ...   <=50K      Human Being          600
#
# [2 rows x 17 columns]


# Select one or more column

print(df.education)

# 0          Bachelors
# 1          Bachelors
# 2            HS-grad
# 3               11th
# 4          Bachelors
#             ...
# 32556     Assoc-acdm
# 32557        HS-grad
# 32558        HS-grad
# 32559        HS-grad
# 32560        HS-grad
# Name: education, Length: 32561, dtype: object

print(df['Age in Month'])

# 0        468
# 1        600
# 2        456
# 3        636
# 4        336
#         ...
# 32556    324
# 32557    480
# 32558    696
# 32559    264
# 32560    624
# Name: Age in Month, Length: 32561, dtype: int64

print(df[['age', 'Kind of Species', 'salary']])

#        age Kind of Species  salary
# 0       39     Human Being   <=50K
# 1       50     Human Being   <=50K
# 2       38     Human Being   <=50K
# 3       53     Human Being   <=50K
# 4       28     Human Being   <=50K
# ...    ...             ...     ...
# 32556   27     Human Being   <=50K
# 32557   40     Human Being    >50K
# 32558   58     Human Being   <=50K
# 32559   22     Human Being   <=50K
# 32560   52     Human Being    >50K

select = ['age', 'education', 'salary']
print(df[select])

#        age    education  salary
# 0       39    Bachelors   <=50K
# 1       50    Bachelors   <=50K
# 2       38      HS-grad   <=50K
# 3       53         11th   <=50K
# 4       28    Bachelors   <=50K
# ...    ...          ...     ...
# 32556   27   Assoc-acdm   <=50K
# 32557   40      HS-grad    >50K
# 32558   58      HS-grad   <=50K
# 32559   22      HS-grad   <=50K
# 32560   52      HS-grad    >50K
#
# [32561 rows x 3 columns]