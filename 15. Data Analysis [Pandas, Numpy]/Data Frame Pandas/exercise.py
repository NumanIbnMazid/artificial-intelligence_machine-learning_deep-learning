import os
# Import pandas Library
import pandas as pd

# Starts Display Customization
pd.set_option('display.width', 320)
# np.set_printoption(linewidth=desired_width)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 50)

# Ends Display Customization

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'RESOURCES')

# Read googleplaystore.csv file in DataFrame
df = pd.read_csv(f"{FILE_DIR}/googleplaystore.csv")
# print(df)
# Display first few records
print(df.head())

#                                                  App  ...   Android Ver
# 0     Photo Editor & Candy Camera & Grid & ScrapBook  ...  4.0.3 and up
# 1                                Coloring book moana  ...  4.0.3 and up
# 2  U Launcher Lite – FREE Live Cool Themes, Hide ...  ...  4.0.3 and up
# 3                              Sketch - Draw & Paint  ...    4.2 and up
# 4              Pixel Draw - Number Art Coloring Book  ...    4.4 and up
#
# [5 rows x 13 columns]

# Find  total records in column and column datatype
info = df.info()
print(info)
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 10841 entries, 0 to 10840
# Data columns (total 13 columns):
#  #   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   App             10841 non-null  object
#  1   Category        10841 non-null  object
#  2   Rating          9367 non-null   float64
#  3   Reviews         10841 non-null  object
#  4   Size            10841 non-null  object
#  5   Installs        10841 non-null  object
#  6   Type            10840 non-null  object
#  7   Price           10841 non-null  object
#  8   Content Rating  10840 non-null  object
#  9   Genres          10841 non-null  object
#  10  Last Updated    10841 non-null  object
#  11  Current Ver     10833 non-null  object
#  12  Android Ver     10838 non-null  object
# dtypes: float64(1), object(12)
# memory usage: 1.1+ MB
# None

# Calculate statastical parameter for numerical colum
description = df.describe()
print(description)
#             Rating
# count  9367.000000
# mean      4.193338
# std       0.537431
# min       1.000000
# 25%       4.000000
# 50%       4.300000
# 75%       4.500000
# max      19.000000

# do any column datatype conversion needed for optimization and better analysis
# Yes, ['Category', 'Type', 'Content Rating', 'Genres', 'Reviews']

df[['Category', 'Type', 'Content Rating', 'Genres']] = df[['Category', 'Type', 'Content Rating', 'Genres']].astype('category')
# df['Reviews'] = df['Reviews'].astype('float')
# ValueError: could not convert string to float: '3.0M'
df['Reviews'] = df['Reviews'].apply(lambda x: x.replace('M', '')).astype('float')
print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 10841 entries, 0 to 10840
# Data columns (total 13 columns):
#  #   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   App             10841 non-null  object
#  1   Category        10841 non-null  category
#  2   Rating          9367 non-null   float64
#  3   Reviews         10841 non-null  float64
#  4   Size            10841 non-null  object
#  5   Installs        10841 non-null  object
#  6   Type            10840 non-null  category
#  7   Price           10841 non-null  object
#  8   Content Rating  10840 non-null  category
#  9   Genres          10841 non-null  category
#  10  Last Updated    10841 non-null  object
#  11  Current Ver     10833 non-null  object
#  12  Android Ver     10838 non-null  object
# dtypes: category(4), float64(2), object(7)
# memory usage: 812.5+ KB
# None


# Which are unique values are there in category column
unique_categories = df['Category'].unique()
print(unique_categories)
# Categories (34, object): [ART_AND_DESIGN, AUTO_AND_VEHICLES, BEAUTY, BOOKS_AND_REFERENCE, ..., VIDEO_PLAYERS, NEWS_AND_MAGAZINES, MAPS_AND_NAVIGATION, 1.9]

# What is average rating of app?
avg_rating = df['Rating'].mean()
print(avg_rating)
# 4.193338315362443

# What is average rating of only those app which comes under Photography category
con1 = df['Category'] == "PHOTOGRAPHY"
avg_rating_photo = df[con1]['Rating'].mean()
print(avg_rating_photo)
# 4.19211356466877

# How many are free and paid apps
free_apps = df['Type'] == "Free"
paid_apps = df['Type'] == "Paid"
counted_free_apps = len(df[free_apps])
counted_paid_apps = len(df[paid_apps])
print(counted_free_apps)
# 10039
print(counted_paid_apps)
# 800

# or
counted_free_paid_apps = df['Type'].value_counts()
print(counted_free_paid_apps)
# Free    10039
# Paid      800
# 0           1
# Name: Type, dtype: int64


# What is average value of Reviews
avg_reviews = df['Reviews'].mean()
print(avg_reviews)
# 444111.9265750392

# Which app has  is maximum Reviews?
max_con = df["Reviews"] == df["Reviews"].max()
max_reviewed = df[max_con]
print(max_reviewed)
#            App Category  Rating     Reviews                Size        Installs  Type Price Content Rating  Genres    Last Updated         Current Ver         Android Ver
# 2544  Facebook   SOCIAL     4.1  78158306.0  Varies with device  1,000,000,000+  Free     0           Teen  Social  August 3, 2018  Varies with device  Varies with device


# select top 5 app which has maximum reviews
sorted_apps = df.sort_values(by='Reviews', ascending=False).head(5)
print(sorted_apps)
#                      App       Category  Rating     Reviews                Size        Installs  Type Price Content Rating         Genres    Last Updated         Current Ver         Android Ver
# 2544            Facebook         SOCIAL     4.1  78158306.0  Varies with device  1,000,000,000+  Free     0           Teen         Social  August 3, 2018  Varies with device  Varies with device
# 3943            Facebook         SOCIAL     4.1  78128208.0  Varies with device  1,000,000,000+  Free     0           Teen         Social  August 3, 2018  Varies with device  Varies with device
# 381   WhatsApp Messenger  COMMUNICATION     4.4  69119316.0  Varies with device  1,000,000,000+  Free     0       Everyone  Communication  August 3, 2018  Varies with device  Varies with device
# 336   WhatsApp Messenger  COMMUNICATION     4.4  69119316.0  Varies with device  1,000,000,000+  Free     0       Everyone  Communication  August 3, 2018  Varies with device  Varies with device
# 3904  WhatsApp Messenger  COMMUNICATION     4.4  69109672.0  Varies with device  1,000,000,000+  Free     0       Everyone  Communication  August 3, 2018  Varies with device  Varies with device

# or (Recommended way)
sorted_reviews = list(df['Reviews'].sort_values(ascending=False).head(5).index)
top_5_reviews = df.iloc[sorted_reviews]
print(top_5_reviews)
#                      App       Category  Rating     Reviews                Size        Installs  Type Price Content Rating         Genres    Last Updated         Current Ver         Android Ver
# 2544            Facebook         SOCIAL     4.1  78158306.0  Varies with device  1,000,000,000+  Free     0           Teen         Social  August 3, 2018  Varies with device  Varies with device
# 3943            Facebook         SOCIAL     4.1  78128208.0  Varies with device  1,000,000,000+  Free     0           Teen         Social  August 3, 2018  Varies with device  Varies with device
# 336   WhatsApp Messenger  COMMUNICATION     4.4  69119316.0  Varies with device  1,000,000,000+  Free     0       Everyone  Communication  August 3, 2018  Varies with device  Varies with device
# 381   WhatsApp Messenger  COMMUNICATION     4.4  69119316.0  Varies with device  1,000,000,000+  Free     0       Everyone  Communication  August 3, 2018  Varies with device  Varies with device
# 3904  WhatsApp Messenger  COMMUNICATION     4.4  69109672.0  Varies with device  1,000,000,000+  Free     0       Everyone  Communication  August 3, 2018  Varies with device  Varies with device


# Select top 5 app which has maximum Installs

# df['Installs'] = df['Installs'].astype('float')
# ValueError: could not convert string to float: '10,000+'
# ValueError: could not convert string to float: 'Free'
err_col = df['Installs'] == 'Free'
print(df[err_col])
#                                            App Category  Rating  Reviews    Size Installs Type     Price Content Rating             Genres Last Updated Current Ver Android Ver
# 10472  Life Made WI-Fi Touchscreen Photo Frame      1.9    19.0      3.0  1,000+     Free    0  Everyone            NaN  February 11, 2018       1.0.19  4.0 and up         NaN
df.drop(labels=10472, axis=0, inplace=True)
df['Installs'] = df['Installs'].apply(lambda x: x.replace(',', '').replace('+', '')).astype('float')
print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# Int64Index: 10840 entries, 0 to 10840
# Data columns (total 13 columns):
#  #   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   App             10840 non-null  object
#  1   Category        10840 non-null  category
#  2   Rating          9366 non-null   float64
#  3   Reviews         10840 non-null  float64
#  4   Size            10840 non-null  object
#  5   Installs        10840 non-null  float64
#  6   Type            10839 non-null  category
#  7   Price           10840 non-null  object
#  8   Content Rating  10840 non-null  category
#  9   Genres          10840 non-null  category
#  10  Last Updated    10840 non-null  object
#  11  Current Ver     10832 non-null  object
#  12  Android Ver     10838 non-null  object
# dtypes: category(4), float64(3), object(6)
# memory usage: 897.0+ KB
# None

sorted_installs = list(df['Installs'].sort_values(ascending=False).head(5).index)
top_5_installs = df.iloc[sorted_installs]
print(top_5_installs)
#               App            Category  Rating    Reviews                Size      Installs  Type Price Content Rating            Genres    Last Updated         Current Ver         Android Ver
# 3765  Google News  NEWS_AND_MAGAZINES     3.9   877635.0                 13M  1.000000e+09  Free     0           Teen  News & Magazines  August 1, 2018               5.2.0          4.4 and up
# 341      Hangouts       COMMUNICATION     4.0  3419249.0  Varies with device  1.000000e+09  Free     0       Everyone     Communication   July 21, 2018  Varies with device  Varies with device
# 9844  Google News  NEWS_AND_MAGAZINES     3.9   878065.0                 13M  1.000000e+09  Free     0           Teen  News & Magazines  August 1, 2018               5.2.0          4.4 and up
# 4096        Gmail       COMMUNICATION     4.3  4604324.0  Varies with device  1.000000e+09  Free     0       Everyone     Communication  August 2, 2018  Varies with device  Varies with device
# 3736  Google News  NEWS_AND_MAGAZINES     3.9   877635.0                 13M  1.000000e+09  Free     0           Teen  News & Magazines  August 1, 2018               5.2.0          4.4 and up