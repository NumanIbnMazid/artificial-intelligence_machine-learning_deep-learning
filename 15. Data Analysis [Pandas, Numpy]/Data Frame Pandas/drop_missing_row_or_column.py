import pandas as pd
import numpy as np

df = pd.DataFrame([[np.nan, 2, np.nan, 0], [3, 4, np.nan, 1],
                    [np.nan, np.nan, np.nan, 5], [np.nan, np.nan, np.nan, np.nan]],
                   columns=list('ABCD'))

print(df)
#      A    B   C    D
# 0  NaN  2.0 NaN  0.0
# 1  3.0  4.0 NaN  1.0
# 2  NaN  NaN NaN  5.0
# 3  NaN  NaN NaN  NaN

dropped_rows_any = df.dropna(axis=0, how='any')  # Remove if any value is NaN

print(dropped_rows_any)

# Empty DataFrame
# Columns: [A, B, C, D]
# Index: []

dropped_rows_all = df.dropna(axis=0, how='all')  # Remove if all values are NaN

print(dropped_rows_all)
#      A    B   C    D
# 0  NaN  2.0 NaN  0.0
# 1  3.0  4.0 NaN  1.0
# 2  NaN  NaN NaN  5.0

print(df)
#      A    B   C    D
# 0  NaN  2.0 NaN  0.0
# 1  3.0  4.0 NaN  1.0
# 2  NaN  NaN NaN  5.0
# 3  NaN  NaN NaN  NaN

dropped_columns_any = df.dropna(axis=1, how='any')  # Remove if any value is NaN

print(dropped_columns_any)

# Empty DataFrame
# Columns: []
# Index: [0, 1, 2, 3]

dropped_columns_all = df.dropna(axis=1, how='all')  # Remove if all values are NaN

print(dropped_columns_all)
#      A    B    D
# 0  NaN  2.0  0.0
# 1  3.0  4.0  1.0
# 2  NaN  NaN  5.0
# 3  NaN  NaN  NaN

print(df)
#      A    B   C    D
# 0  NaN  2.0 NaN  0.0
# 1  3.0  4.0 NaN  1.0
# 2  NaN  NaN NaN  5.0
# 3  NaN  NaN NaN  NaN

# Remove specific row or column
dropped_row = df.drop(axis=0, labels=[2])
print(dropped_row)
#      A    B   C    D
# 0  NaN  2.0 NaN  0.0
# 1  3.0  4.0 NaN  1.0
# 3  NaN  NaN NaN  NaN

dropped_column = df.drop(axis=1, labels=['A'])
print(dropped_column)
#      B   C    D
# 0  2.0 NaN  0.0
# 1  4.0 NaN  1.0
# 2  NaN NaN  5.0
# 3  NaN NaN  NaN

