import os
import pandas as pd
import numpy as np

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

df_head = df.head()
df_tail = df.tail()

df_values = df.values
df_shape = df.shape
df_summary = df.describe()
df_columns = df.columns
df_data_types = df.dtypes

print(df_summary)
#                 age        fnlwgt  ...  capital-loss  hours-per-week
# count  32561.000000  3.256100e+04  ...  32561.000000    32561.000000
# mean      38.581647  1.897784e+05  ...     87.303830       40.437456
# std       13.640433  1.055500e+05  ...    402.960219       12.347429
# min       17.000000  1.228500e+04  ...      0.000000        1.000000
# 25%       28.000000  1.178270e+05  ...      0.000000       40.000000
# 50%       37.000000  1.783560e+05  ...      0.000000       40.000000
# 75%       48.000000  2.370510e+05  ...      0.000000       45.000000
# max       90.000000  1.484705e+06  ...   4356.000000       99.000000
#
# [8 rows x 6 columns]

print(df_columns)
# Index(['age', 'workclass', 'fnlwgt', 'education', 'education-num',
#        'marital-status', 'occupation', 'relationship', 'race', 'sex',
#        'capital-gain', 'capital-loss', 'hours-per-week', 'native-country',
#        'salary'],
#       dtype='object')

print(df_data_types)
# age                int64
# workclass         object
# fnlwgt             int64
# education         object
# education-num      int64
# marital-status    object
# occupation        object
# relationship      object
# race              object
# sex               object
# capital-gain       int64
# capital-loss       int64
# hours-per-week     int64
# native-country    object
# salary            object
# dtype: object

df_info = df.info()
print(df_info)
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 32561 entries, 0 to 32560
# Data columns (total 15 columns):
#  #   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   age             32561 non-null  int64
#  1   workclass       32561 non-null  object
#  2   fnlwgt          32561 non-null  int64
#  3   education       32561 non-null  object
#  4   education-num   32561 non-null  int64
#  5   marital-status  32561 non-null  object
#  6   occupation      32561 non-null  object
#  7   relationship    32561 non-null  object
#  8   race            32561 non-null  object
#  9   sex             32561 non-null  object
#  10  capital-gain    32561 non-null  int64
#  11  capital-loss    32561 non-null  int64
#  12  hours-per-week  32561 non-null  int64
#  13  native-country  32561 non-null  object
#  14  salary          32561 non-null  object
# dtypes: int64(6), object(9)
# memory usage: 3.7+ MB
# None
