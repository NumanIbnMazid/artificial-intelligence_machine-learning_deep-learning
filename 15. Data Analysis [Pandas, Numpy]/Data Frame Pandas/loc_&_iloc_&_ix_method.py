import os
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv", index_col='country'
)

print(df)
#              beer_servings  ...  continent
# country                     ...
# Afghanistan              0  ...         AS
# Albania                 89  ...         EU
# Algeria                 25  ...         AF
# Andorra                245  ...         EU
# Angola                 217  ...         AF
# ...                    ...  ...        ...
# Venezuela              333  ...         SA
# Vietnam                111  ...         AS
# Yemen                    6  ...         AS
# Zambia                  32  ...         AF
# Zimbabwe                64  ...         AF
#
# [193 rows x 5 columns]

print(df.loc['Albania'])
# beer_servings                    89
# spirit_servings                 132
# wine_servings                    54
# total_litres_of_pure_alcohol    4.9
# continent                        EU
# Name: Albania, dtype: object

print(df.loc[['Albania', 'India']])
#          beer_servings  spirit_servings  ...  total_litres_of_pure_alcohol  continent
# country                                  ...
# Albania             89              132  ...                           4.9         EU
# India                9              114  ...                           2.2         AS
#
# [2 rows x 5 columns]

print(df.loc[['Albania', 'India']][['beer_servings', 'continent']])
#          beer_servings continent
# country
# Albania             89        EU
# India                9        AS

print(df.loc['Algeria':'Australia'])
#                    beer_servings  ...  continent
# country                           ...
# Algeria                       25  ...         AF
# Andorra                      245  ...         EU
# Angola                       217  ...         AF
# Antigua & Barbuda            102  ...        NaN
# Argentina                    193  ...         SA
# Armenia                       21  ...         EU
# Australia                    261  ...         OC

# iloc method (positional based indexing)

print(df.iloc[0])
# beer_servings                    0
# spirit_servings                  0
# wine_servings                    0
# total_litres_of_pure_alcohol     0
# continent                       AS
# Name: Afghanistan, dtype: object

print(df.iloc[5:10])
#                    beer_servings  ...  continent
# country                           ...
# Antigua & Barbuda            102  ...        NaN
# Argentina                    193  ...         SA
# Armenia                       21  ...         EU
# Australia                    261  ...         OC
# Austria                      279  ...         EU
#
# [5 rows x 5 columns]

# ix method (Both Positional and label based argument)
# print(df.ix[2]) # This is deprecated. Use .loc or .iloc



