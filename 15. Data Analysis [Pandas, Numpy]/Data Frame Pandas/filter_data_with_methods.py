import os
import pandas as pd
import numpy as np

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

# Get unique objects of a column in data frame

print(df['education'].unique())
# [' Bachelors' ' HS-grad' ' 11th' ' Masters' ' 9th' ' Some-college'
#  ' Assoc-acdm' ' Assoc-voc' ' 7th-8th' ' Doctorate' ' Prof-school'
#  ' 5th-6th' ' 10th' ' 1st-4th' ' Preschool' ' 12th']

# isin method

degree_holders = df['education'].isin([' 11th', ' 12th', ' Masters'])

print(df[degree_holders])
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 3       53            Private  234721  ...             40   United-States   <=50K
# 5       37            Private  284582  ...             40   United-States   <=50K
# 8       31            Private   45781  ...             50   United-States    >50K
# 18      38            Private   28887  ...             50   United-States   <=50K
# 19      43   Self-emp-not-inc  292175  ...             45   United-States    >50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 32522   58            Private  147707  ...             40   United-States   <=50K
# 32535   22            Private  325033  ...             35   United-States   <=50K
# 32544   31            Private  199655  ...             30   United-States   <=50K
# 32553   32            Private  116138  ...             11          Taiwan   <=50K
# 32554   53            Private  321865  ...             40   United-States    >50K


# Between method

age_between = df['age'].between(40, 60)
print(df[age_between])
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 1       50   Self-emp-not-inc   83311  ...             13   United-States   <=50K
# 3       53            Private  234721  ...             40   United-States   <=50K
# 6       49            Private  160187  ...             16         Jamaica   <=50K
# 7       52   Self-emp-not-inc  209642  ...             45   United-States    >50K
# 9       42            Private  159449  ...             40   United-States    >50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 32552   43            Private   84661  ...             45   United-States   <=50K
# 32554   53            Private  321865  ...             40   United-States    >50K
# 32557   40            Private  154374  ...             40   United-States    >50K
# 32558   58            Private  151910  ...             40   United-States   <=50K
# 32560   52       Self-emp-inc  287927  ...             40   United-States    >50K
#
# [11905 rows x 15 columns]


# Between date range will also work

