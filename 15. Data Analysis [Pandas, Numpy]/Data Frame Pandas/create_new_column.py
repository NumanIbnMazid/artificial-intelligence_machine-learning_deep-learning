import os
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

df["Kind of Species"] = "Human Being"

print(df.head(2))
#    age          workclass  fnlwgt  ...  native-country  salary Kind of Species
# 0   39          State-gov   77516  ...   United-States   <=50K     Human Being
# 1   50   Self-emp-not-inc   83311  ...   United-States   <=50K     Human Being

df["Age in Month"] = df["age"] * 12

print(df.head(2))
#    age          workclass  fnlwgt  ...  salary  Kind of Species Age in Month
# 0   39          State-gov   77516  ...   <=50K      Human Being          468
# 1   50   Self-emp-not-inc   83311  ...   <=50K      Human Being          600
#
# [2 rows x 17 columns]