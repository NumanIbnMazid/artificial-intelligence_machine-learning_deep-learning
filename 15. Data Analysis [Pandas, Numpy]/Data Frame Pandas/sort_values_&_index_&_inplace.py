import os
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

print(df['native-country'].unique())
# [' United-States' ' Cuba' ' Jamaica' ' India' ' ?' ' Mexico' ' South'
#  ' Puerto-Rico' ' Honduras' ' England' ' Canada' ' Germany' ' Iran'
#  ' Philippines' ' Italy' ' Poland' ' Columbia' ' Cambodia' ' Thailand'
#  ' Ecuador' ' Laos' ' Taiwan' ' Haiti' ' Portugal' ' Dominican-Republic'
#  ' El-Salvador' ' France' ' Guatemala' ' China' ' Japan' ' Yugoslavia'
#  ' Peru' ' Outlying-US(Guam-USVI-etc)' ' Scotland' ' Trinadad&Tobago'
#  ' Greece' ' Nicaragua' ' Vietnam' ' Hong' ' Ireland' ' Hungary'
#  ' Holand-Netherlands']

sorted_values = df.sort_values(by='native-country', ascending=True)

print(sorted_values)
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 21541   27            Private  169958  ...             40               ?   <=50K
# 18277   90            Private  311184  ...             20               ?   <=50K
# 27305   34   Self-emp-not-inc  114185  ...             50               ?   <=50K
# 5854    40            Private   37618  ...             40               ?   <=50K
# 27299   37   Self-emp-not-inc  327164  ...             70               ?   <=50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 23814   41   Self-emp-not-inc  145441  ...             40      Yugoslavia   <=50K
# 7287    35            Private  164526  ...             40      Yugoslavia    >50K
# 31519   29            Private  273051  ...             52      Yugoslavia    >50K
# 4447    25            Private  191230  ...             40      Yugoslavia   <=50K
# 18247   41            Private  324629  ...             40      Yugoslavia   <=50K
#
# [32561 rows x 15 columns]

sorted_age = df.sort_values(by='age')
print(sorted_age)
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 12318   17            Private  127366  ...              8   United-States   <=50K
# 6312    17            Private  132755  ...             15   United-States   <=50K
# 30927   17            Private  108470  ...             17   United-States   <=50K
# 12787   17          Local-gov  308901  ...             15   United-States   <=50K
# 25755   17                  ?   47407  ...             10   United-States   <=50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 24043   90   Self-emp-not-inc   82628  ...             12   United-States   <=50K
# 32277   90            Private  313749  ...             25   United-States   <=50K
# 5104    90            Private   52386  ...             35   United-States   <=50K
# 8963    90                  ?   77053  ...             40   United-States   <=50K
# 10210   90   Self-emp-not-inc  282095  ...             40   United-States   <=50K
#
# [32561 rows x 15 columns]

# Sort Multiple values

sorted_df = df.sort_values(by=['native-country', 'age'], ascending=[False, True])
print(sorted_df)
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 6328    20            Private  175069  ...             40      Yugoslavia   <=50K
# 30543   22            Private  180060  ...             40      Yugoslavia   <=50K
# 4447    25            Private  191230  ...             40      Yugoslavia   <=50K
# 31519   29            Private  273051  ...             52      Yugoslavia    >50K
# 12816   31            Private  182177  ...             40      Yugoslavia   <=50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 16108   74            Private  194312  ...             10               ?   <=50K
# 5201    76   Self-emp-not-inc   33213  ...             30               ?    >50K
# 12830   81            Private  201398  ...             60               ?   <=50K
# 32525   81                  ?  120478  ...              1               ?   <=50K
# 18277   90            Private  311184  ...             20               ?   <=50K
#
# [32561 rows x 15 columns]


# Sort Indexing

df.sort_values('native-country', inplace=True)  # permanent modification

print(df.sort_index(ascending=False))
#        age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 32560   52       Self-emp-inc  287927  ...             40   United-States    >50K
# 32559   22            Private  201490  ...             20   United-States   <=50K
# 32558   58            Private  151910  ...             40   United-States   <=50K
# 32557   40            Private  154374  ...             40   United-States    >50K
# 32556   27            Private  257302  ...             38   United-States   <=50K
# ...    ...                ...     ...  ...            ...             ...     ...
# 4       28            Private  338409  ...             40            Cuba   <=50K
# 3       53            Private  234721  ...             40   United-States   <=50K
# 2       38            Private  215646  ...             40   United-States   <=50K
# 1       50   Self-emp-not-inc   83311  ...             13   United-States   <=50K
# 0       39          State-gov   77516  ...             40   United-States   <=50K
#
# [32561 rows x 15 columns]