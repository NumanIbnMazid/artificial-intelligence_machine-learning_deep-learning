# Create Data Frame

import pandas as pd
import numpy as np
import os

data = np.random.randn(5, 3)
print(data)
# [[ 0.62159557 -0.31558767 -0.06754326]
#  [ 0.48783919 -1.37961368 -0.36196715]
#  [ 0.14125886 -0.04266215 -0.0515798 ]
#  [ 1.34325025  0.58685947  0.07663948]
#  [ 0.29092642  0.30663122 -0.37031655]]

df = pd.DataFrame(data)
print(df)
#           0         1         2
# 0  0.704481  0.107107  0.328012
# 1 -1.310530  0.506589  1.264178
# 2  0.516256 -0.106846 -1.592326
# 3  0.081523 -2.054955 -1.585173
# 4 -0.906576 -0.036268 -0.652792

# Modifying columns

df = pd.DataFrame(data, columns=["A", "B", "C"])
print(df)
#           A         B         C
# 0 -1.892529  0.872514  0.744677
# 1  1.494094 -0.452763 -0.138585
# 2  0.690008  1.729336  1.774707
# 3 -0.048834  0.917386  0.024876
# 4  1.386457  0.638626 -0.934915

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

# Data Link: http://archive.ics.uci.edu/ml/datasets/Adult
columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

file = pd.read_csv(f"{FILE_DIR}/adult.data.csv", header=None, names=columns)
print(file)