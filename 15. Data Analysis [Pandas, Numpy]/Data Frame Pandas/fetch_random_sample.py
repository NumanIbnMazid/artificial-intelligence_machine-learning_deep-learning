import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

print(df.sample(n=1))
#       age workclass  fnlwgt  ... hours-per-week  native-country  salary
# 1932   28   Private  162551  ...             48           China

print(df.sample(frac=0.01))  # Percentage of data ex: 0.1 for 10%
#        age     workclass  fnlwgt  ... hours-per-week  native-country  salary
# 4230    26       Private  182178  ...             40   United-States   <=50K
# 22329   56     Local-gov  216824  ...             40     Philippines    >50K
# 13655   49     State-gov  391585  ...             40   United-States    >50K
# 13223   28       Private  155621  ...             40        Columbia   <=50K
# 11728   30       Private  170130  ...             40   United-States   <=50K
# ...    ...           ...     ...  ...            ...             ...     ...
# 28537   47       Private  158286  ...             50   United-States    >50K
# 15488   43       Private   55395  ...             40   United-States   <=50K
# 5834    58       Private  209423  ...             38            Cuba   <=50K
# 30773   38   Federal-gov  190895  ...             40               ?    >50K
# 7772    58       Private  186991  ...             40   United-States   <=50K
#
# [326 rows x 15 columns]

