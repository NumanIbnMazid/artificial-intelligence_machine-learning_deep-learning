import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

print(df.info())

df['sex'] = df['sex'].astype('category')

print(df['sex'])
# 0           Male
# 1           Male
# 2           Male
# 3           Male
# 4         Female
#           ...
# 32556     Female
# 32557       Male
# 32558     Female
# 32559       Male
# 32560     Female
# Name: sex, Length: 32561, dtype: category
# Categories (2, object): [Female, Male]

print(df.info())  # Memory use reduced
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 32561 entries, 0 to 32560
# Data columns (total 15 columns):
#   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   age             32561 non-null  int64
#  1   workclass       32561 non-null  object
#  2   fnlwgt          32561 non-null  int64
#  3   education       32561 non-null  object
#  4   education-num   32561 non-null  int64
#  5   marital-status  32561 non-null  object
#  6   occupation      32561 non-null  object
#  7   relationship    32561 non-null  object
#  8   race            32561 non-null  object
#  9   sex             32561 non-null  category
#  10  capital-gain    32561 non-null  int64
#  11  capital-loss    32561 non-null  int64
#  12  hours-per-week  32561 non-null  int64
#  13  native-country  32561 non-null  object
#  14  salary          32561 non-null  object
# dtypes: category(1), int64(6), object(8)
# memory usage: 3.5+ MB
# None

df['marital-status'] = df['marital-status'].astype('category')

print(df.info())  # Memory use reduced
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 32561 entries, 0 to 32560
# Data columns (total 15 columns):
#  #   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   age             32561 non-null  int64
#  1   workclass       32561 non-null  object
#  2   fnlwgt          32561 non-null  int64
#  3   education       32561 non-null  object
#  4   education-num   32561 non-null  int64
#  5   marital-status  32561 non-null  category
#  6   occupation      32561 non-null  object
#  7   relationship    32561 non-null  object
#  8   race            32561 non-null  object
#  9   sex             32561 non-null  category
#  10  capital-gain    32561 non-null  int64
#  11  capital-loss    32561 non-null  int64
#  12  hours-per-week  32561 non-null  int64
#  13  native-country  32561 non-null  object
#  14  salary          32561 non-null  object
# dtypes: category(2), int64(6), object(7)
# memory usage: 3.3+ MB
# None

df['age'] = df['age'].astype('float')

print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 32561 entries, 0 to 32560
# Data columns (total 15 columns):
#  #   Column          Non-Null Count  Dtype
# ---  ------          --------------  -----
#  0   age             32561 non-null  float64
#  1   workclass       32561 non-null  object
#  2   fnlwgt          32561 non-null  int64
#  3   education       32561 non-null  object
#  4   education-num   32561 non-null  int64
#  5   marital-status  32561 non-null  category
#  6   occupation      32561 non-null  object
#  7   relationship    32561 non-null  object
#  8   race            32561 non-null  object
#  9   sex             32561 non-null  category
#  10  capital-gain    32561 non-null  int64
#  11  capital-loss    32561 non-null  int64
#  12  hours-per-week  32561 non-null  int64
#  13  native-country  32561 non-null  object
#  14  salary          32561 non-null  object
# dtypes: category(2), float64(1), int64(5), object(7)
# memory usage: 3.3+ MB
# None