import os
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

df['age'] = df['age'] + 5

print(df.head(2))

#    age          workclass  fnlwgt  ... hours-per-week  native-country  salary
# 0   44          State-gov   77516  ...             40   United-States   <=50K
# 1   55   Self-emp-not-inc   83311  ...             13   United-States   <=50K