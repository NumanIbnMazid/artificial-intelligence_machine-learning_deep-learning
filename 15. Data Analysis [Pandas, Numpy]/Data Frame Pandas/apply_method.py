import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

print(df['salary'].unique())
# [' <=50K' ' >50K']


def encode_salary(salary):
    if salary == ' <=50':
        return '1'
    else:
        return '2'


df['encoded_salary'] = df['salary'].apply(encode_salary)

print(df['encoded_salary'])
# 0        2
# 1        2
# 2        2
# 3        2
# 4        2
#         ..
# 32556    2
# 32557    2
# 32558    2
# 32559    2
# 32560    2
# Name: encoded_salary, Length: 32561, dtype: object

print(df.head())
#    age          workclass  fnlwgt  ...  native-country  salary encoded_salary
# 0   39          State-gov   77516  ...   United-States   <=50K              2
# 1   50   Self-emp-not-inc   83311  ...   United-States   <=50K              2
# 2   38            Private  215646  ...   United-States   <=50K              2
# 3   53            Private  234721  ...   United-States   <=50K              2
# 4   28            Private  338409  ...            Cuba   <=50K              2
#
# [5 rows x 16 columns]

# Apply on multiple column

print(df.keys())
# Index(['age', 'workclass', 'fnlwgt', 'education', 'education-num',
#        'marital-status', 'occupation', 'relationship', 'race', 'sex',
#        'capital-gain', 'capital-loss', 'hours-per-week', 'native-country',
#        'salary', 'encoded_salary'],
#       dtype='object')


def desc(row):
    age = row[0]
    sex = row[9]
    salary = row[14]
    description = sex + " earning " + salary + " $"
    return description


df['description'] = df.apply(func=desc, axis='columns')

print(df)
#        age          workclass  ...  encoded_salary               description
# 0       39          State-gov  ...               2     Male earning  <=50K $
# 1       50   Self-emp-not-inc  ...               2     Male earning  <=50K $
# 2       38            Private  ...               2     Male earning  <=50K $
# 3       53            Private  ...               2     Male earning  <=50K $
# 4       28            Private  ...               2   Female earning  <=50K $
# ...    ...                ...  ...             ...                       ...
# 32556   27            Private  ...               2   Female earning  <=50K $
# 32557   40            Private  ...               2      Male earning  >50K $
# 32558   58            Private  ...               2   Female earning  <=50K $
# 32559   22            Private  ...               2     Male earning  <=50K $
# 32560   52       Self-emp-inc  ...               2    Female earning  >50K $
#
# [32561 rows x 17 columns]

