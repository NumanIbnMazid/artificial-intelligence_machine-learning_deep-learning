import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

columns = [
    "age", "workclass", "fnlwgt", "education", "education-num", "marital-status",
    "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",
    "hours-per-week", "native-country", "salary"
]

df = pd.read_csv(
    f"{FILE_DIR}/adult.data.csv", header=None, names=columns
)

# Usage One

# df = pd.read_csv(
#     f"{FILE_DIR}/adult.data.csv", header=None, names=columns, index_col='native-country'
# )

# or
# Usage two

# df.set_index('native-country', inplace=True)

# print(df.head(3))

#                 age          workclass  ...  hours-per-week  salary
# native-country                          ...
#  United-States   39          State-gov  ...              40   <=50K
#  United-States   50   Self-emp-not-inc  ...              13   <=50K
#  United-States   38            Private  ...              40   <=50K
#
# [3 rows x 14 columns]

df.set_index(['age', 'native-country'], inplace=True)

print(df.head(3))
#
#                             workclass  fnlwgt  ... hours-per-week  salary
# age native-country                             ...
# 39   United-States          State-gov   77516  ...             40   <=50K
# 50   United-States   Self-emp-not-inc   83311  ...             13   <=50K
# 38   United-States            Private  215646  ...             40   <=50K
#
# [3 rows x 13 columns]

