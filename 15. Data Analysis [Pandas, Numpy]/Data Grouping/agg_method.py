import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(f"{FILE_DIR}/sales_data_sample-edited.csv")
columns = df.columns
grouped_country = df.groupby('COUNTRY')

# Get sum (Calculates only numerical columns)
print(grouped_country.agg('sum'))
#              QUANTITYORDERED  PRICEEACH       SALES  MONTH_ID  YEAR_ID
# COUNTRY
# Australia               6246   15449.14   630623.10      1345   370706
# Austria                 1974    4759.16   202062.53       383   110211
# Belgium                 1074    2887.31   108412.62       188    66138
# Canada                  2293    5957.05   224078.56       545   140274
# Denmark                 2197    5503.89   245637.15       444   126228
# Finland                 3192    7722.15   329581.91       485   184374
# France                 11090   25920.87  1110916.52      1960   629233
# Germany                 2148    5184.30   220472.09       525   124227
# Ireland                  490    1377.98    57756.43        81    32064
# Italy                   3773    9329.40   374674.31       868   226422
# Japan                   1842    4290.26   188167.81       254   104218
# Norway                  2842    7318.18   307463.70       710   170287
# Philippines              961    2144.99    94015.73       158    52082
# Singapore               2760    6581.90   288488.41       564   158278
# Spain                  12429   28042.54  1215686.92      2299   685341
# Sweden                  2006    4943.51   210014.21       503   114218
# Switzerland             1078    2713.09   117713.56       164    62124
# UK                      5013   11882.70   478880.46      1166   288533
# USA                    35659   84159.65  3627982.83      7380  2011812

print(grouped_country.agg('sum')[['QUANTITYORDERED', 'SALES']])
#              QUANTITYORDERED       SALES
# COUNTRY
# Australia               6246   630623.10
# Austria                 1974   202062.53
# Belgium                 1074   108412.62
# Canada                  2293   224078.56
# Denmark                 2197   245637.15
# Finland                 3192   329581.91
# France                 11090  1110916.52
# Germany                 2148   220472.09
# Ireland                  490    57756.43
# Italy                   3773   374674.31
# Japan                   1842   188167.81
# Norway                  2842   307463.70
# Philippines              961    94015.73
# Singapore               2760   288488.41
# Spain                  12429  1215686.92
# Sweden                  2006   210014.21
# Switzerland             1078   117713.56
# UK                      5013   478880.46
# USA                    35659  3627982.83

print(grouped_country.agg(['sum', 'mean'])[['QUANTITYORDERED', 'SALES']])
#             QUANTITYORDERED                  SALES
#                         sum       mean         sum         mean
# COUNTRY
# Australia              6246  33.762162   630623.10  3408.773514
# Austria                1974  35.890909   202062.53  3673.864182
# Belgium                1074  32.545455   108412.62  3285.230909
# Canada                 2293  32.757143   224078.56  3201.122286
# Denmark                2197  34.873016   245637.15  3899.002381
# Finland                3192  34.695652   329581.91  3582.412065
# France                11090  35.318471  1110916.52  3537.950701
# Germany                2148  34.645161   220472.09  3556.001452
# Ireland                 490  30.625000    57756.43  3609.776875
# Italy                  3773  33.389381   374674.31  3315.701858
# Japan                  1842  35.423077   188167.81  3618.611731
# Norway                 2842  33.435294   307463.70  3617.220000
# Philippines             961  36.961538    94015.73  3615.989615
# Singapore              2760  34.936709   288488.41  3651.752025
# Spain                 12429  36.342105  1215686.92  3554.640117
# Sweden                 2006  35.192982   210014.21  3684.459825
# Switzerland            1078  34.774194   117713.56  3797.211613
# UK                     5013  34.812500   478880.46  3325.558750
# USA                   35659  35.516932  3627982.83  3613.528715

req = {
    'QUANTITYORDERED': 'mean',
    'SALES': 'sum'
}
print(grouped_country.agg(req))
#              QUANTITYORDERED       SALES
# COUNTRY
# Australia          33.762162   630623.10
# Austria            35.890909   202062.53
# Belgium            32.545455   108412.62
# Canada             32.757143   224078.56
# Denmark            34.873016   245637.15
# Finland            34.695652   329581.91
# France             35.318471  1110916.52
# Germany            34.645161   220472.09
# Ireland            30.625000    57756.43
# Italy              33.389381   374674.31
# Japan              35.423077   188167.81
# Norway             33.435294   307463.70
# Philippines        36.961538    94015.73
# Singapore          34.936709   288488.41
# Spain              36.342105  1215686.92
# Sweden             35.192982   210014.21
# Switzerland        34.774194   117713.56
# UK                 34.812500   478880.46
# USA                35.516932  3627982.83