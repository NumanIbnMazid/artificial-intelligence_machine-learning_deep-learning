import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(f"{FILE_DIR}/sales_data_sample-edited.csv")

print(df)

columns = df.columns
print(columns)
# Index(['QUANTITYORDERED', 'PRICEEACH', 'SALES', 'STATUS', 'MONTH_ID',
#        'YEAR_ID', 'PRODUCTLINE', 'CUSTOMERNAME', 'CITY', 'COUNTRY',
#        'TERRITORY', 'DEALSIZE'],
#       dtype='object')

grouped_country = df.groupby('COUNTRY')
print(grouped_country)
# <pandas.core.groupby.generic.DataFrameGroupBy object at 0x7f469a602910>

usa_data = grouped_country.get_group('USA')
print(usa_data)
#       QUANTITYORDERED  PRICEEACH    SALES  ... COUNTRY  TERRITORY  DEALSIZE
# 0                  30      95.70  2871.00  ...     USA        NaN     Small
# 3                  45      83.26  3746.70  ...     USA        NaN    Medium
# 4                  49     100.00  5205.27  ...     USA        NaN    Medium
# 5                  36      96.66  3479.76  ...     USA        NaN    Medium
# 8                  22      98.57  2168.54  ...     USA        NaN     Small
# ...               ...        ...      ...  ...     ...        ...       ...
# 2804               28      64.43  1804.04  ...     USA        NaN     Small
# 2807               36      63.34  2280.24  ...     USA        NaN     Small
# 2809               23      65.52  1506.96  ...     USA        NaN     Small
# 2817               42      97.16  4080.72  ...     USA        NaN    Medium
# 2822               47      65.52  3079.44  ...     USA        NaN    Medium
#
# [1004 rows x 12 columns]


# Get Size
print(len(usa_data))
# 1004

print(grouped_country.size())
# COUNTRY
# Australia       185
# Austria          55
# Belgium          33
# Canada           70
# Denmark          63
# Finland          92
# France          314
# Germany          62
# Ireland          16
# Italy           113
# Japan            52
# Norway           85
# Philippines      26
# Singapore        79
# Spain           342
# Sweden           57
# Switzerland      31
# UK              144
# USA            1004
# dtype: int64

# First record of all country
print(grouped_country.first())
#              QUANTITYORDERED  PRICEEACH  ...  TERRITORY DEALSIZE
# COUNTRY                                  ...
# Australia                 37     100.00  ...       APAC   Medium
# Austria                   41     100.00  ...       EMEA    Large
# Belgium                   30     100.00  ...       EMEA   Medium
# Canada                    47     100.00  ...        NaN    Large
# Denmark                   50     100.00  ...       EMEA    Large
# Finland                   23     100.00  ...       EMEA    Small
# France                    34      81.35  ...       EMEA    Small
# Germany                   21     100.00  ...       EMEA   Medium
# Ireland                   32     100.00  ...       EMEA    Large
# Italy                     34     100.00  ...       EMEA    Large
# Japan                     32     100.00  ...      Japan    Large
# Norway                    48     100.00  ...       EMEA   Medium
# Philippines               33     100.00  ...      Japan   Medium
# Singapore                 45     100.00  ...      Japan    Large
# Spain                     66     100.00  ...       EMEA    Large
# Sweden                    29     100.00  ...       EMEA    Large
# Switzerland               27     100.00  ...       EMEA   Medium
# UK                        24     100.00  ...       EMEA    Small
# USA                       30      95.70  ...        NaN    Small

# Last record of all country
print(grouped_country.last())
#              QUANTITYORDERED  PRICEEACH  ...  TERRITORY DEALSIZE
# COUNTRY                                  ...
# Australia                 42      57.61  ...       APAC    Small
# Austria                   28      48.17  ...       EMEA    Small
# Belgium                   37      45.86  ...       EMEA    Small
# Canada                    33      51.32  ...        NaN    Small
# Denmark                   37      86.74  ...       EMEA   Medium
# Finland                   29     100.00  ...       EMEA   Medium
# France                    34      62.24  ...       EMEA    Small
# Germany                   21      45.19  ...       EMEA    Small
# Ireland                   35     100.00  ...       EMEA   Medium
# Italy                     32      60.06  ...       EMEA    Small
# Japan                     27      76.31  ...      Japan    Small
# Norway                    32      64.41  ...       EMEA    Small
# Philippines               40      63.67  ...      Japan    Small
# Singapore                 50      43.68  ...      Japan    Small
# Spain                     43     100.00  ...       EMEA   Medium
# Sweden                    38      48.59  ...       EMEA    Small
# Switzerland               40      88.12  ...       EMEA   Medium
# UK                        35      59.51  ...       EMEA    Small
# USA                       47      65.52  ...        NaN   Medium
#
# [19 rows x 11 columns]

