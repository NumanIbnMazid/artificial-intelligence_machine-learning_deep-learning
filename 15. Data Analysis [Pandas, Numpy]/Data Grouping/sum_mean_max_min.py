import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(f"{FILE_DIR}/sales_data_sample-edited.csv")
columns = df.columns
grouped_country = df.groupby('COUNTRY')

# Get sum
print(grouped_country.get_group('USA').sum())
# QUANTITYORDERED                                                35659
# PRICEEACH                                                    84159.6
# SALES                                                    3.62798e+06
# STATUS             ShippedShippedShippedShippedShippedShippedShip...
# MONTH_ID                                                        7380
# YEAR_ID                                                      2011812
# PRODUCTLINE        MotorcyclesMotorcyclesMotorcyclesMotorcyclesMo...
# CUSTOMERNAME       Land of Toys Inc.Toys4GrownUps.comCorporate Gi...
# CITY               NYCPasadenaSan FranciscoBurlingameSan Francisc...
# COUNTRY            USAUSAUSAUSAUSAUSAUSAUSAUSAUSAUSAUSAUSAUSAUSAU...
# TERRITORY                                                          0
# DEALSIZE           SmallMediumMediumMediumSmallSmallMediumMediumM...
# dtype: object

# Sum of specific column

print(grouped_country.get_group('USA').sum()['SALES'])
# 3627982.83

# Get sum of all country
print(grouped_country.sum())
#              QUANTITYORDERED  PRICEEACH       SALES  MONTH_ID  YEAR_ID
# COUNTRY
# Australia               6246   15449.14   630623.10      1345   370706
# Austria                 1974    4759.16   202062.53       383   110211
# Belgium                 1074    2887.31   108412.62       188    66138
# Canada                  2293    5957.05   224078.56       545   140274
# Denmark                 2197    5503.89   245637.15       444   126228
# Finland                 3192    7722.15   329581.91       485   184374
# France                 11090   25920.87  1110916.52      1960   629233
# Germany                 2148    5184.30   220472.09       525   124227
# Ireland                  490    1377.98    57756.43        81    32064
# Italy                   3773    9329.40   374674.31       868   226422
# Japan                   1842    4290.26   188167.81       254   104218
# Norway                  2842    7318.18   307463.70       710   170287
# Philippines              961    2144.99    94015.73       158    52082
# Singapore               2760    6581.90   288488.41       564   158278
# Spain                  12429   28042.54  1215686.92      2299   685341
# Sweden                  2006    4943.51   210014.21       503   114218
# Switzerland             1078    2713.09   117713.56       164    62124
# UK                      5013   11882.70   478880.46      1166   288533
# USA                    35659   84159.65  3627982.83      7380  2011812

# Get specific column sum of all country
print(grouped_country.sum()['SALES'])
# COUNTRY
# Australia       630623.10
# Austria         202062.53
# Belgium         108412.62
# Canada          224078.56
# Denmark         245637.15
# Finland         329581.91
# France         1110916.52
# Germany         220472.09
# Ireland          57756.43
# Italy           374674.31
# Japan           188167.81
# Norway          307463.70
# Philippines      94015.73
# Singapore       288488.41
# Spain          1215686.92
# Sweden          210014.21
# Switzerland     117713.56
# UK              478880.46
# USA            3627982.83
# Name: SALES, dtype: float64

# Get mean
print(grouped_country.get_group('USA').mean()['SALES'])
# 3613.5287151394423

# Max
print(grouped_country.get_group('USA').max()['SALES'])
# 14082.8

# Min
print(grouped_country.get_group('USA').min()['SALES'])
# 541.14