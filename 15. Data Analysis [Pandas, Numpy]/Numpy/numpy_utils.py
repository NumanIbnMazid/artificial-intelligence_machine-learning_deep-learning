import numpy as np

a = np.random.random(size=(3, 3))
b = np.random.random(size=(3, 3))

print(a)
# [[0.39454086 0.71699105 0.11188227]
# [0.36903118 0.32424093 0.69779792]
# [0.39739768 0.79113655 0.73022977]]
print(b)
#[[0.249063   0.05523637 0.80427521]
# [0.70218398 0.40240437 0.69401741]
# [0.01849899 0.71173853 0.56800757]]


h_stacked = np.hstack((a, b)) # Horizontal Stack
print(h_stacked)
# [[0.63271782 0.34377913 0.29025973 0.31350597 0.84638262 0.03186159]
# [0.40139953 0.02901762 0.01701846 0.60475227 0.87079455 0.82372788]
# [0.99766613 0.11425542 0.91619102 0.32159836 0.64658857 0.27622028]]


v_stacked = np.vstack((a, b))  # Vertical Stack
print(v_stacked)
# [[0.13664839 0.88358818 0.83341149]
#  [0.23224494 0.91586442 0.25451692]
#  [0.08867329 0.06961182 0.24918047]
#  [0.75635958 0.63579071 0.97180074]
#  [0.95537387 0.44256328 0.85215811]
#  [0.12403185 0.62543142 0.78455477]]

matrix = a * 10
new_mat = np.floor(matrix)
print(np.floor(matrix))  # Get whole number/round value (8.23423 becomes 8)
# [[5. 7. 1.]
#  [5. 3. 5.]
#  [9. 0. 7.]]
print(np.ceil(matrix)) # Round to upper integer (8.1232 becomes 9)
# [[6.  8.  2.]
#  [6.  4.  6.]
#  [10.  1.  8.]]

print(new_mat)
# [[3. 8. 5.]
#  [7. 4. 4.]
#  [7. 5. 2.]]

print(np.sum(new_mat, axis=0)) # Sum column wise
# [17. 17. 11.]
print(np.sum(new_mat, axis=1)) # Sum row wise
# [16. 15. 14.]

print(np.mean(new_mat, axis=0))


# Cumulative Sum
print(np.cumsum(new_mat))
# Cumulative Product
print(np.cumproduct(new_mat))


# Find all unique values
print(np.unique(new_mat))

print(np.min(new_mat))
print(np.min(new_mat, axis=0))
print(np.max(new_mat, axis=1))


new_arr = np.arange(20, 27)
print(new_arr)
# [20 21 22 23 24 25 26]


for index, item in enumerate(new_arr):
    print(index, item)
# 0 20
# 1 21
# 2 22
# 3 23
# 4 24
# 5 25
# 6 26
