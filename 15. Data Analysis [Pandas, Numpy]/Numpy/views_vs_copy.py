import numpy as np

a = np.array([2, 5, 0, 10])

b = a

print(a, b)
# [ 2  5  0 10] [ 2  5  0 10]

a[0] = -5

print(a, b)
# [-5  5  0 10] [-5  5  0 10] Overrriding b

# check memory address 
print(id(a), id(b))
# 140338081396896 140338081396896 same memory address, not creating new one

c = a.view()

print(a, b, c)
# [-5  5  0 10][-5  5  0 10][-5  5  0 10]

print(id(a), id(b), id(c))
# 140684317255920 140684317255920 140684316136672 c is taking new memory address

c[2] = 200

print(a, b, c)
# [-5   5 200  10][-5   5 200  10][-5   5 200  10] All three are same


# Solution

d = a.copy()

print(a, b, c, d)
# [-5   5 200  10][-5   5 200  10][-5   5 200  10][-5   5 200  10]
print(id(a), id(b), id(c), id(d))
# 139851670173936 139851670173936 139851669054368 139851462295344
d[0] = 500
print(a, b, c, d)
# [-5   5 200  10][-5   5 200  10][-5   5 200  10][500   5 200  10]
