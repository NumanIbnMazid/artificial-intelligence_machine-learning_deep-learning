import numpy as np

a = [1, 4, 4.5, 'string', True]

print(a)
# [1, 4, 4.5, 'string', True]

print(np.array(a))
# ['1' '4' '4.5' 'string' 'True'] All Converts to string type (Homogeneous Type)

# performance faster in numpy array