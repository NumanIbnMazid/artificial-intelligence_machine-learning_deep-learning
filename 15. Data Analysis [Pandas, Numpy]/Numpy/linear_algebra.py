import numpy as np

mat = np.array([[1,2], [3, 4]])
print(mat)
# [[1 2]
# [3 4]]

# Find determinate of matrix : (1*4) - (3*2)
print(np.linalg.det(mat))
# -2.0000000000000004

# Find inverse of matrix
print(np.linalg.inv(mat))
# [[-2.   1. ]
# [1.5 - 0.5]]

# Transpose matrix
print(mat.transpose())
# [[1 3]
# [2 4]]

print(np.trace(mat))  # (1 + 4)
# 5


arr1 = np.array([[1, 2], [3, 4]])
arr2 = np.array([[1, 2], [3, 4]])

print(arr1)
# [[1 2]
#  [3 4]]
print(arr2)
# [[1 2]
#  [3 4]]

# Matrix Multiplication

print(arr1 * arr2)
# [[ 1  4]
# [9 16]]

# Real Matrix Multiplication
print(arr1.dot(arr2))
# [[ 7 10]
# [15 22]]


# Linear sets of two equations
# 2x + y = 1
# x + y = 2
# Find value of x and y

M0 = np.array([[2,1], [1,1]]) # Left hand side equation
M1 = np.array([1, 2]) # Right side equation (for 2, 1)

print(np.linalg.solve(M0, M1))  # [-1. 3.]


# Equation 2
# 3x - y = 3
# x + y = 5

MQ1 = np.array([[3,-1], [1,1]])
MQ2 = np.array([3, 5])

print(np.linalg.solve(MQ1, MQ2))  # [2. 3.]


# Eig Vector
eigvalues, eigvectors = np.linalg.eig(mat)
print(eigvalues) 
# [-0.37228132  5.37228132]
print(eigvectors)
# [[-0.82456484 -0.41597356]
# [0.56576746 - 0.90937671]]


# Singular value decomposition
C0 = np.array([[1,3,5], [3,-1,6]])
U, sigma, V = np.linalg.svd(C0)
print(U, "U")
# [-0.76822128  0.6401844]]
print(sigma, "sigma")
# [8.42614977 3.16227766]
print(V, "V")
# [[-3.49489188e-01 - 1.36756639e-01 - 9.26906106e-01]
#  [3.64399349e-01 - 9.31242780e-01  7.35637502e-17]
#  [-8.63174619e-01 - 3.37763981e-01  3.75293313e-01]]

U, sigma, V = np.linalg.svd(C0, full_matrices=False)
print(U, "U")
# [-0.76822128  0.6401844 ]]
print(sigma, "sigma")
# [8.42614977 3.16227766]
print(V, "V")
# [[-3.49489188e-01 - 1.36756639e-01 - 9.26906106e-01]
#  [3.64399349e-01 - 9.31242780e-01  7.35637502e-17]]
