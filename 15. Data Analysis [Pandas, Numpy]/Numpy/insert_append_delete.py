import numpy as np


a = np.arange(10, 19)

print(a)
# [10 11 12 13 14 15 16 17 18]

b = np.append(a, [21, 22])

print(b)
# [10 11 12 13 14 15 16 17 18 21 22]
print(a)
# [10 11 12 13 14 15 16 17 18] No effect in main array

c = a.reshape(3,3)

print(c)
# [[10 11 12]
#  [13 14 15]
#  [16 17 18]]

d = np.append(c, [50, 51, 52])

print(d)
# [10 11 12 13 14 15 16 17 18 50 51 52]
print(c)
# [[10 11 12]
#  [13 14 15]
#  [16 17 18]]

e = np.append(c, [[50, 51, 52]], axis=0) # Rows wise
print(e)
# [[10 11 12]
#  [13 14 15]
#  [16 17 18]
#  [50 51 52]]

f = np.append(c, [[50], [51], [52]], axis=1)  # Column wise
print(f)
# [[10 11 12 50]
#  [13 14 15 51]
#  [16 17 18 52]]

g = np.insert(a, 1, -100)
print(g)
# [10 - 100   11   12   13   14   15   16   17   18]


print(c)
# [[10 11 12]
#  [13 14 15]
#  [16 17 18]]


h = np.insert(c, 1, -100)
print(h)
# [10 - 100   11   12   13   14   15   16   17   18]

i = np.insert(c, 1, -100, axis=0)
print(i)
# [[10   11   12]
#  [-100 - 100 - 100]
#  [13   14   15]
#  [16   17   18]]

j = np.insert(c, 1, -100, axis=1)
print(j)
# [[10 - 100   11   12]
#  [13 - 100   14   15]
#  [16 - 100   17   18]]


print(a)
# [10 11 12 13 14 15 16 17 18]

k = np.delete(a, 2)
print(k)
# [10 11 13 14 15 16 17 18]


l = np.delete(c, 1, axis=0)
print(l)
# [[10 11 12]
#  [16 17 18]]
m = np.delete(c, 1, axis=1)
print(m)
# [[10 12]
#  [13 15]
#  [16 18]]
