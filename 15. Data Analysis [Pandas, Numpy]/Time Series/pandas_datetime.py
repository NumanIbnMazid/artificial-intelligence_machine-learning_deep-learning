import pandas as pd
import datetime as dt

print(pd.Timestamp('2020-12-01'))
# 2020-12-01 00:00:00
print(pd.Timestamp('2020/12/01'))
# 2020-12-01 00:00:00
print(pd.Timestamp('2020/12/01 11:58:30'))
# 2020-12-01 11:58:30

today = dt.date(2020, 4, 9)
now = dt.datetime(2020, 4, 9, 11, 50, 30)

print(pd.Timestamp(today))
# 2020-04-09 00:00:00
print(pd.Timestamp(now))
# 2020-04-09 11:50:30

# Datetime Index
print(pd.DatetimeIndex(['2020-01-01']))
# DatetimeIndex(['2020-01-01'], dtype='datetime64[ns]', freq=None)
print(pd.DatetimeIndex(['2020-01-01', '2020-01-02', '2020-01-03']))
# DatetimeIndex(['2020-01-01', '2020-01-02', '2020-01-03'], dtype='datetime64[ns]', freq=None)
print(pd.DatetimeIndex(['2020-01-01', '2020/01/02', '3rd jan 2020', '2019/12/31']))
# DatetimeIndex(['2020-01-01', '2020-01-02', '2020-01-03', '2019-12-31'], dtype='datetime64[ns]', freq=None)

print(pd.to_datetime('2020, 1, 13'))
# 2020-01-13 00:00:00
print(pd.to_datetime(['2020-01-01', '2020-01-02', '2020-01-03']))
# DatetimeIndex(['2020-01-01', '2020-01-02', '2020-01-03'], dtype='datetime64[ns]', freq=None)

dt_val = pd.to_datetime(['2020-01-01', '2020-01-02', '2020-01-03'])
time_series = pd.Series(dt_val)
print(time_series)
# 0   2020-01-01
# 1   2020-01-02
# 2   2020-01-03
# dtype: datetime64[ns]

print(time_series.dt)
# <pandas.core.indexes.accessors.DatetimeProperties object at 0x7fbf0cd20bb0>
print(time_series.dt.date)
# 0    2020-01-01
# 1    2020-01-02
# 2    2020-01-03
# dtype: object
print(time_series.dt.day)
# 0    1
# 1    2
# 2    3
# dtype: int64
print(time_series.dt.day_name())
# 0    Wednesday
# 1     Thursday
# 2       Friday
# dtype: object
print(time_series.dt.dayofweek)
# 0    2
# 1    3
# 2    4
# dtype: int64