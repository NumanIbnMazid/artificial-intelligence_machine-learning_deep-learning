import datetime as dt

today = dt.date(2020, 4, 9)
print(today)
# 2020-04-09
print(today.year)
# 2020
print(today.month)
# 4
print(today.day)
# 9
print(today.isoweekday())
# 4
print(str(today))
# 2020-04-09

now = dt.datetime(2020, 4, 9, 11, 50, 30)
print(now)
# 2020-04-09 11:50:30
print(now.hour)
# 11
print(now.minute)
# 50
print(now.second)
# 30

