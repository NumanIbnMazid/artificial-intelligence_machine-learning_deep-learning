import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(f"{FILE_DIR}/SMSSpamCollection", sep="\t", header=None, names=['Status', 'Message'])

print(df)
#      Status                                            Message
# 0       ham  Go until jurong point, crazy.. Available only ...
# 1       ham                      Ok lar... Joking wif u oni...
# 2      spam  Free entry in 2 a wkly comp to win FA Cup fina...
# 3       ham  U dun say so early hor... U c already then say...
# 4       ham  Nah I don't think he goes to usf, he lives aro...
# ...     ...                                                ...
# 5567   spam  This is the 2nd time we have tried 2 contact u...
# 5568    ham               Will ü b going to esplanade fr home?
# 5569    ham  Pity, * was in mood for that. So...any other s...
# 5570    ham  The guy did some bitching but I acted like i'd...
# 5571    ham                         Rofl. Its true to its name
#
# [5572 rows x 2 columns]

# Set status to upper case
df['Status'] = df['Status'].str.upper()
print(df)

# Set status to Title
df['Status'] = df['Status'].str.title()
print(df)

print(df['Message'].str.len())
# 0       111
# 1        29
# 2       155
# 3        49
# 4        61
#        ...
# 5567    160
# 5568     36
# 5569     57
# 5570    125
# 5571     26
# Name: Message, Length: 5572, dtype: int64

print(df['Message'][3])
# U dun say so early hor... U c already then say...

print(df['Message'][3].replace("...", ''))
# U dun say so early hor U c already then say

# .strip() to remove extra spaces

# Stop words = a, an the etc. remove those words : Stop removal

print(df['Message'].str.startswith('Pity'))
# 0       False
# 1       False
# 2       False
# 3       False
# 4       False
#         ...
# 5567    False
# 5568    False
# 5569     True
# 5570    False
# 5571    False
# Name: Message, Length: 5572, dtype: bool

print(df['Message'].str.endswith('...'))
# 0        True
# 1        True
# 2       False
# 3        True
# 4       False
#         ...
# 5567    False
# 5568    False
# 5569    False
# 5570    False
# 5571    False
# Name: Message, Length: 5572, dtype: bool


# Filter message with string

mask = df['Message'].str.contains('Sorry')
sorry_msg = df[mask]
print(sorry_msg)
#      Status                                            Message
# 57      Ham                 Sorry, I'll call later in meeting.
# 63      Ham  Sorry my roommates took forever, it ok if I co...
# 80      Ham                             Sorry, I'll call later
# 91      Ham  Sorry to be a pain. Is it ok if we meet anothe...
# 223     Ham                             Sorry, I'll call later
# ...     ...                                                ...
# 5135    Ham                        Sorry * was at the grocers.
# 5191    Ham                             Sorry, I'll call later
# 5423    Ham                             Sorry, I'll call later
# 5458    Ham                             Sorry, I'll call later
# 5558    Ham                             Sorry, I'll call later

print(len(sorry_msg))
# 111

print(sorry_msg[sorry_msg['Status'] == 'Spam'])
#      Status                                            Message
# 1699   Spam  Free msg. Sorry, a service you ordered from 81...
# 2915   Spam  Sorry! U can not unsubscribe yet. THE MOB offe...
# 3360   Spam  Sorry I missed your call let's talk when you h...


# Splitting text

print(df['Message'].str.split()[0])
# ['Go', 'until', 'jurong', 'point,', 'crazy..', 'Available', 'only', 'in', 'bugis', 'n', 'great', 'world', 'la', 'e', 'buffet...', 'Cine', 'there', 'got', 'amore', 'wat...']

print(df['Message'].str.split().get(0))


# Apply on columns
print(df.columns)
# Index(['Status', 'Message'], dtype='object')
print(df.columns.str.len())
# Int64Index([6, 7], dtype='int64')

# Make columns as title
df.columns = df.columns.str.title()

print(df.head(2))
#   Status                                            Message
# 0    Ham  Go until jurong point, crazy.. Available only ...
# 1    Ham                      Ok lar... Joking wif u oni...

