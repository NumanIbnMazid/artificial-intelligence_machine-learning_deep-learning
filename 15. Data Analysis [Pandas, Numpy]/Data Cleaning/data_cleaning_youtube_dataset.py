import pandas as pd
import os

# Starts Display Customization
desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 50)
# Ends Display Customization

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'RESOURCES')

df = pd.read_csv(f"{FILE_DIR}/top-5000-youtube-channels-data-from-socialblade.csv")

print(df.head(3))
#   Rank Grade                Channel name Video Uploads Subscribers  Video views
# 0  1st  A++                       Zee TV         82757    18752951  20869786591
# 1  2nd  A++                     T-Series         12661    61196302  47548839843
# 2  3rd  A++   Cocomelon - Nursery Rhymes           373    19238251   9793305082

print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 5000 entries, 0 to 4999
# Data columns (total 6 columns):
#  #   Column         Non-Null Count  Dtype
# ---  ------         --------------  -----
#  0   Rank           5000 non-null   object
#  1   Grade          5000 non-null   object
#  2   Channel name   5000 non-null   object
#  3   Video Uploads  5000 non-null   object
#  4   Subscribers    5000 non-null   object
#  5   Video views    5000 non-null   int64
# dtypes: int64(1), object(5)
# memory usage: 234.5+ KB

# print(df['Rank'].head(50))
# 0      1st
# 1      2nd
# 2      3rd
# 3      4th
# 4      5th
# 5      6th
# 6      7th
# 7      8th
# 8      9th
# 9     10th
# 10    11th
# 11    12th
# 12    13th
# 13    14th
# 14    15th
# 15    16th
# 16    17th
# 17    18th
# 18    19th
# 19    20th
# 20    21st
# 21    22nd
# 22    23rd
# 23    24th
# 24    25th
# 25    26th
# 26    27th
# 27    28th
# 28    29th
# 29    30th
# 30    31st
# 31    32nd
# 32    33rd
# 33    34th
# 34    35th
# 35    36th
# 36    37th
# 37    38th
# 38    39th
# 39    40th
# 40    41st
# 41    42nd
# 42    43rd
# 43    44th
# 44    45th
# 45    46th
# 46    47th
# 47    48th
# 48    49th
# 49    50th
# Name: Rank, dtype: object
# ValueError: invalid literal for int() with base 10: '1,000'

# def remove_chars(x):
#     chars = ['st', 'nd', 'rd', 'th', ',']
#     for char in chars:
#         if char in x:
#             x = x.replace(char, '')
#     return x
#
#
# ranks = df['Rank']
# df['Rank'] = df['Rank'].apply(remove_chars)
# print(df.head())
#   Rank Grade                Channel name Video Uploads Subscribers  Video views
# 0    1  A++                       Zee TV         82757    18752951  20869786591
# 1    2  A++                     T-Series         12661    61196302  47548839843
# 2    3  A++   Cocomelon - Nursery Rhymes           373    19238251   9793305082
# 3    4  A++                    SET India         27323    31180559  22675948293
# 4    5  A++                          WWE         36756    32852346  26273668433

df['Rank'] = df['Rank'].str[0:-2].str.replace(',', '').astype('int')
print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 5000 entries, 0 to 4999
# Data columns (total 6 columns):
#  #   Column         Non-Null Count  Dtype
# ---  ------         --------------  -----
#  0   Rank           5000 non-null   int64
#  1   Grade          5000 non-null   object
#  2   Channel name   5000 non-null   object
#  3   Video Uploads  5000 non-null   object
#  4   Subscribers    5000 non-null   object
#  5   Video views    5000 non-null   int64
# dtypes: int64(2), object(4)
# memory usage: 234.5+ KB

print(df['Grade'].unique())
# ['A++ ' 'A+ ' 'A ' '\xa0 ' 'A- ' 'B+ ']

df['Grade'] = df['Grade'].astype('category')

# df['Video Uploads'] = df['Video Uploads'].astype('int')
# ValueError: invalid literal for int() with base 10: '--'
print(df[df['Video Uploads'].str.contains('-') == True])
#       Rank Grade                  Channel name Video Uploads Subscribers  Video views
# 267    268                    MidnightXChannel            --         --     190256974
# 517    518                      Dusama Pets TV            --         --      91601494
# 2323  2324                              Random            --       12275     17897584
# 3072  3073        Boram Tube Toy Shcool [보람튜브…            --      726527    205555289
# 3247  3248                       atheer sultan            --         --      79663674
# 4898  4899                        ExzoticSlice            --       99785      9745292

# mask1 = df[df['Video Uploads'].str.contains('-')].index
# print(mask1)
# Int64Index([267, 517, 2323, 3072, 3247, 4898], dtype='int64')
# df.drop(labels=mask1, axis=0, inplace=True)

df['Video Uploads'] = df['Video Uploads'].str.strip().replace('--', '0')
df['Video Uploads'] = df['Video Uploads'].astype('int')

df['Subscribers'] = df['Subscribers'].str.strip().replace('--', '0')
df['Subscribers'] = df['Subscribers'].astype('int')

print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 5000 entries, 0 to 4999
# Data columns (total 6 columns):
#  #   Column         Non-Null Count  Dtype
# ---  ------         --------------  -----
#  0   Rank           5000 non-null   int64
#  1   Grade          5000 non-null   category
#  2   Channel name   5000 non-null   object
#  3   Video Uploads  5000 non-null   int64
#  4   Subscribers    5000 non-null   int64
#  5   Video views    5000 non-null   int64
# dtypes: category(1), int64(4), object(1)
# memory usage: 200.5+ KB

print(df[df['Channel name'].str.startswith(' ')])
Index: []
print(df[df['Channel name'].str.endswith(' ')])
# Index: []

# No Cleaning Required but can be uppercase or lowercase
df['Channel name'] = df['Channel name'].str.lower().str.strip()

print(df.head(10))
#    Rank Grade                Channel name  Video Uploads  Subscribers  Video views
# 0     1  A++                       zee tv          82757     18752951  20869786591
# 1     2  A++                     t-series          12661     61196302  47548839843
# 2     3  A++   cocomelon - nursery rhymes            373     19238251   9793305082
# 3     4  A++                    set india          27323     31180559  22675948293
# 4     5  A++                          wwe          36756     32852346  26273668433
# 5     6  A++                   movieclips          30243     17149705  16618094724
# 6     7  A++                   netd müzik           8500     11373567  23898730764
# 7     8  A++        abs-cbn entertainment         100147     12149206  17202609850
# 8     9  A++              ryan toysreview           1140     16082927  24518098041
# 9    10  A++                  zee marathi          74607      2841811   2591830307


# ['A++ ' 'A+ ' 'A ' '\xa0 ' 'A- ' 'B+ ']
channel_map = {
    "A++ ": 5,
    'A+ ': 4,
    'A ': 3,
    'A- ': 2,
    'B+ ': 1,
}

df['Grade'] = df['Grade'].map(channel_map)
print(df.info())
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 5000 entries, 0 to 4999
# Data columns (total 6 columns):
#  #   Column         Non-Null Count  Dtype
# ---  ------         --------------  -----
#  0   Rank           5000 non-null   int64
#  1   Grade          4994 non-null   float64
#  2   Channel name   5000 non-null   object
#  3   Video Uploads  5000 non-null   int64
#  4   Subscribers    5000 non-null   int64
#  5   Video views    5000 non-null   int64
# dtypes: float64(1), int64(4), object(1)
# memory usage: 234.5+ KB

print(df.head(10))

#    Rank  Grade                Channel name  Video Uploads  Subscribers  Video views
# 0     1    5.0                      zee tv          82757     18752951  20869786591
# 1     2    5.0                    t-series          12661     61196302  47548839843
# 2     3    5.0  cocomelon - nursery rhymes            373     19238251   9793305082
# 3     4    5.0                   set india          27323     31180559  22675948293
# 4     5    5.0                         wwe          36756     32852346  26273668433
# 5     6    5.0                  movieclips          30243     17149705  16618094724
# 6     7    5.0                  netd müzik           8500     11373567  23898730764
# 7     8    5.0       abs-cbn entertainment         100147     12149206  17202609850
# 8     9    5.0             ryan toysreview           1140     16082927  24518098041
# 9    10    5.0                 zee marathi          74607      2841811   2591830307
