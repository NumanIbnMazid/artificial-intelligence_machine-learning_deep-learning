import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline  # (If in notebook)

# Starts Display Customization
desired_width = 320

pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 50)

# Ends Display Customization

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, 'RESOURCES')

