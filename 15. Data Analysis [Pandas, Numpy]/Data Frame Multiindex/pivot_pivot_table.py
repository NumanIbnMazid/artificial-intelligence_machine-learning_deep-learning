import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv"
)

df_new = df.pivot(
    index='country', columns='continent', values=['spirit_servings']
)

print(df_new)
#             spirit_servings
# continent               NaN    AF   AS     EU  OC     SA
# country
# Afghanistan             NaN   NaN  0.0    NaN NaN    NaN
# Albania                 NaN   NaN  NaN  132.0 NaN    NaN
# Algeria                 NaN   0.0  NaN    NaN NaN    NaN
# Andorra                 NaN   NaN  NaN  138.0 NaN    NaN
# Angola                  NaN  57.0  NaN    NaN NaN    NaN
# ...                     ...   ...  ...    ...  ..    ...
# Venezuela               NaN   NaN  NaN    NaN NaN  100.0
# Vietnam                 NaN   NaN  2.0    NaN NaN    NaN
# Yemen                   NaN   NaN  0.0    NaN NaN    NaN
# Zambia                  NaN  19.0  NaN    NaN NaN    NaN
# Zimbabwe                NaN  18.0  NaN    NaN NaN    NaN


df_new_2 = df.pivot(
    index='country', columns='continent', values=['spirit_servings', 'beer_servings']
)

print(df_new_2)
#             spirit_servings                    ... beer_servings
# continent               NaN    AF   AS     EU  ...            AS     EU  OC     SA
# country                                        ...
# Afghanistan             NaN   NaN  0.0    NaN  ...           0.0    NaN NaN    NaN
# Albania                 NaN   NaN  NaN  132.0  ...           NaN   89.0 NaN    NaN
# Algeria                 NaN   0.0  NaN    NaN  ...           NaN    NaN NaN    NaN
# Andorra                 NaN   NaN  NaN  138.0  ...           NaN  245.0 NaN    NaN
# Angola                  NaN  57.0  NaN    NaN  ...           NaN    NaN NaN    NaN
# ...                     ...   ...  ...    ...  ...           ...    ...  ..    ...
# Venezuela               NaN   NaN  NaN    NaN  ...           NaN    NaN NaN  333.0
# Vietnam                 NaN   NaN  2.0    NaN  ...         111.0    NaN NaN    NaN
# Yemen                   NaN   NaN  0.0    NaN  ...           6.0    NaN NaN    NaN
# Zambia                  NaN  19.0  NaN    NaN  ...           NaN    NaN NaN    NaN
# Zimbabwe                NaN  18.0  NaN    NaN  ...           NaN    NaN NaN    NaN


print(df.pivot_table(index='continent', values=['spirit_servings'], aggfunc='mean'))

#            spirit_servings
# continent
# AF               16.339623
# AS               60.840909
# EU              132.555556
# OC               58.437500
# SA              114.750000

