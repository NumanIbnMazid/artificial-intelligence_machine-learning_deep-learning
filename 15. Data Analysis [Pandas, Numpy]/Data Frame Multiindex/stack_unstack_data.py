import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv", index_col=['country', 'continent']
)

# Stack and Unstack -> Column to index and index to column

stacked = df.stack()

print(stacked)
# country      continent
# Afghanistan  AS         beer_servings                    0.0
#                         spirit_servings                  0.0
#                         wine_servings                    0.0
#                         total_litres_of_pure_alcohol     0.0
# Albania      EU         beer_servings                   89.0
#                                                         ...
# Zambia       AF         total_litres_of_pure_alcohol     2.5
# Zimbabwe     AF         beer_servings                   64.0
#                         spirit_servings                 18.0
#                         wine_servings                    4.0
#                         total_litres_of_pure_alcohol     4.7
# Length: 772, dtype: float64


unstacked = df.unstack()
print(unstacked)
#             beer_servings                           ... total_litres_of_pure_alcohol
# continent             NaN     AF     AS     EU  OC  ...                           AF   AS    EU  OC   SA
# country                                             ...
# Afghanistan           NaN    NaN    0.0    NaN NaN  ...                          NaN  0.0   NaN NaN  NaN
# Albania               NaN    NaN    NaN   89.0 NaN  ...                          NaN  NaN   4.9 NaN  NaN
# Algeria               NaN   25.0    NaN    NaN NaN  ...                          0.7  NaN   NaN NaN  NaN
# Andorra               NaN    NaN    NaN  245.0 NaN  ...                          NaN  NaN  12.4 NaN  NaN
# Angola                NaN  217.0    NaN    NaN NaN  ...                          5.9  NaN   NaN NaN  NaN
# ...                   ...    ...    ...    ...  ..  ...                          ...  ...   ...  ..  ...
# Venezuela             NaN    NaN    NaN    NaN NaN  ...                          NaN  NaN   NaN NaN  7.7
# Vietnam               NaN    NaN  111.0    NaN NaN  ...                          NaN  2.0   NaN NaN  NaN
# Yemen                 NaN    NaN    6.0    NaN NaN  ...                          NaN  0.1   NaN NaN  NaN
# Zambia                NaN   32.0    NaN    NaN NaN  ...                          2.5  NaN   NaN NaN  NaN
# Zimbabwe              NaN   64.0    NaN    NaN NaN  ...                          4.7  NaN   NaN NaN  NaN


print(df.unstack(0))

#           beer_servings                          ... total_litres_of_pure_alcohol
# country     Afghanistan Albania Algeria Andorra  ...                      Vietnam Yemen Zambia Zimbabwe
# continent                                        ...
# NaN                 NaN     NaN     NaN     NaN  ...                          NaN   NaN    NaN      NaN
# AF                  NaN     NaN    25.0     NaN  ...                          NaN   NaN    2.5      4.7
# AS                  0.0     NaN     NaN     NaN  ...                          2.0   0.1    NaN      NaN
# EU                  NaN    89.0     NaN   245.0  ...                          NaN   NaN    NaN      NaN
# OC                  NaN     NaN     NaN     NaN  ...                          NaN   NaN    NaN      NaN
# SA                  NaN     NaN     NaN     NaN  ...                          NaN   NaN    NaN      NaN
#
# [6 rows x 772 columns]

