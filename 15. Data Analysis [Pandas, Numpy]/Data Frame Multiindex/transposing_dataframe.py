import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv"
)

df.set_index(keys=['continent', 'country'], inplace=True)

# Transpose: Column -> to -> row, Row -> to -> column (Vertical to Horizontal and opposite)

print(df.transpose())
# continent                             AS      EU      AF  ...    AS     AF
# country                      Afghanistan Albania Algeria  ... Yemen Zambia Zimbabwe
# beer_servings                        0.0    89.0    25.0  ...   6.0   32.0     64.0
# spirit_servings                      0.0   132.0     0.0  ...   0.0   19.0     18.0
# wine_servings                        0.0    54.0    14.0  ...   0.0    4.0      4.0
# total_litres_of_pure_alcohol         0.0     4.9     0.7  ...   0.1    2.5      4.7

# Grab information in the same way

# Swapping level (Swap Indexes or Axis)

swapped = df.swaplevel()
print(swapped)

#                        beer_servings  ...  total_litres_of_pure_alcohol
# country     continent                 ...
# Afghanistan AS                     0  ...                           0.0
# Albania     EU                    89  ...                           4.9
# Algeria     AF                    25  ...                           0.7
# Andorra     EU                   245  ...                          12.4
# Angola      AF                   217  ...                           5.9
# ...                              ...  ...                           ...
# Venezuela   SA                   333  ...                           7.7
# Vietnam     AS                   111  ...                           2.0
# Yemen       AS                     6  ...                           0.1
# Zambia      AF                    32  ...                           2.5
# Zimbabwe    AF                    64  ...                           4.7

