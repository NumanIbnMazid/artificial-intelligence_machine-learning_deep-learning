import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv"
)

df.set_index(keys=['continent', 'country'], inplace=True)

print(df.head(3))
#                        beer_servings  ...  total_litres_of_pure_alcohol
# continent country                     ...
# AS        Afghanistan              0  ...                           0.0
# EU        Albania                 89  ...                           4.9
# AF        Algeria                 25  ...                           0.7

