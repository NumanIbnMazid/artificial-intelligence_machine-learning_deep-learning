import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv"
)

df.set_index(keys=['continent', 'country'], inplace=True)

print(df.loc['AS'].head(3))
#              beer_servings  ...  total_litres_of_pure_alcohol
# country                     ...
# Afghanistan              0  ...                           0.0
# Bahrain                 42  ...                           2.0
# Bangladesh               0  ...                           0.0

print(df.loc['AS', 'Bangladesh'])
# beer_servings                   0.0
# spirit_servings                 0.0
# wine_servings                   0.0
# total_litres_of_pure_alcohol    0.0
# Name: (AS, Bangladesh), dtype: float64

print(df.loc[('AS', 'Bangladesh')])
# beer_servings                   0.0
# spirit_servings                 0.0
# wine_servings                   0.0
# total_litres_of_pure_alcohol    0.0
# Name: (AS, Bangladesh), dtype: float64

print(df.loc[('AS', 'Bangladesh'), 'wine_servings'])
# 0.0

print(df.iloc[0].head(3))
# beer_servings      0.0
# spirit_servings    0.0
# wine_servings      0.0
# Name: (AS, Afghanistan), dtype: float64

