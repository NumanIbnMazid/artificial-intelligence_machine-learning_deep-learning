import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv"
)

df.set_index(keys=['continent', 'country'], inplace=True)

sorted_index = df.sort_index()
print(sorted_index)
#                                         beer_servings  ...  total_litres_of_pure_alcohol
# continent country                                      ...
# AF        Algeria                                  25  ...                           0.7
#           Angola                                  217  ...                           5.9
#           Benin                                    34  ...                           1.1
#           Botswana                                173  ...                           5.4
#           Burkina Faso                             25  ...                           4.3
# ...                                               ...  ...                           ...
# NaN       St. Kitts & Nevis                       194  ...                           7.7
#           St. Lucia                               171  ...                          10.1
#           St. Vincent & the Grenadines            120  ...                           6.3
#           Trinidad & Tobago                       197  ...                           6.4
#           USA                                     249  ...                           8.7

sorted_index_reverse = df.sort_index(ascending=False)

sorted_index_reverse_two = df.sort_index(ascending=[True, False])  #continent # country
print(sorted_index_reverse_two)
#                              beer_servings  ...  total_litres_of_pure_alcohol
# continent country                           ...
# AF        Zimbabwe                      64  ...                           4.7
#           Zambia                        32  ...                           2.5
#           Uganda                        45  ...                           8.3
#           Tunisia                       51  ...                           1.3
#           Togo                          36  ...                           1.3
# ...                                    ...  ...                           ...
# NaN       Canada                       240  ...                           8.2
#           Belize                       263  ...                           6.8
#           Barbados                     143  ...                           6.3
#           Bahamas                      122  ...                           6.3
#           Antigua & Barbuda            102  ...                           4.9



