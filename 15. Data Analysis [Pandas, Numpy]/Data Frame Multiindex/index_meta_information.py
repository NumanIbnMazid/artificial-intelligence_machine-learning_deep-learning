import pandas as pd
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FILE_DIR = os.path.join(BASE_DIR, "RESOURCES")

df = pd.read_csv(
    f"{FILE_DIR}/drinks.csv"
)

df.set_index(keys=['continent', 'country'], inplace=True)

indexes = df.index
print(indexes)
# MultiIndex([('AS',       'Afghanistan'),
#             ('EU',           'Albania'),
#             ('AF',           'Algeria'),
#             ('EU',           'Andorra'),
#             ('AF',            'Angola'),
#             ( nan, 'Antigua & Barbuda'),
#             ('SA',         'Argentina'),
#             ('EU',           'Armenia'),
#             ('OC',         'Australia'),
#             ('EU',           'Austria'),
#             ...
#             ('AF',          'Tanzania'),
#             ( nan,               'USA'),
#             ('SA',           'Uruguay'),
#             ('AS',        'Uzbekistan'),
#             ('OC',           'Vanuatu'),
#             ('SA',         'Venezuela'),
#             ('AS',           'Vietnam'),
#             ('AS',             'Yemen'),
#             ('AF',            'Zambia'),
#             ('AF',          'Zimbabwe')],
#            names=['continent', 'country'], length=193)

# Get index names
index_names = df.index.names
print(index_names)
# ['continent', 'country']

print(df.index[0])
# ('AS', 'Afghanistan')

print(df.index.get_level_values(0))
# Index(['AS', 'EU', 'AF', 'EU', 'AF',  nan, 'SA', 'EU', 'OC', 'EU',
#        ...
#        'AF',  nan, 'SA', 'AS', 'OC', 'SA', 'AS', 'AS', 'AF', 'AF'],
#       dtype='object', name='continent', length=193)

print(df.index.get_level_values('country'))
# Index(['Afghanistan', 'Albania', 'Algeria', 'Andorra', 'Angola',
#        'Antigua & Barbuda', 'Argentina', 'Armenia', 'Australia', 'Austria',
#        ...
#        'Tanzania', 'USA', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela',
#        'Vietnam', 'Yemen', 'Zambia', 'Zimbabwe'],
#       dtype='object', name='country', length=193)

