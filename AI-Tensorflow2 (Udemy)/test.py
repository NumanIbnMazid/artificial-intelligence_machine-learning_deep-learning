 
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

x = np.linspace(0, 10*np.pi, 1000)
y = np.sin(x)

plt.plot(x, y)

plt.show()

print(tf.__version__)