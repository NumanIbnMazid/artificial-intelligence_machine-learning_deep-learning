import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import csv

get_ipython().run_line_magic('matplotlib', 'inline')

# Starts Display Customization
desired_width = 320

pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 50)

plt.style.use("fivethirtyeight")
# sns.set(style="whitegrid", palette="rainbow")
sns.set(style="darkgrid", palette="icefire")
# Ends Display Customization

BASE_DIR = os.path.normpath(os.getcwd() + os.sep + os.pardir)
DATA_DIR = os.path.join(BASE_DIR, 'DATA')