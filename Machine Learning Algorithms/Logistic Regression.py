
# ### Logistic Regression is Classification Algorithm
# #### Used to predict Discrete/Categorical values

# ## Problem: Predict if a person will buy an SUV car based on their age and estimated salary.

# #### 1. Load Libraries

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
# get_ipython().run_line_magic('matplotlib', 'inline')


# #### Import the dataset and extract the independent and dependent variable

# ##### Import Dataset

# In[2]:


social_network = pd.read_csv("Social_Network_Ads.csv")


# In[3]:


social_network.head()


# ##### Extracting independent variable (Age, Salary)

# In[4]:


X = social_network.iloc[:, [2, 3]].values


# In[5]:


print(X)


# ##### Extracting dependent variable (Purchased)

# In[6]:


y = social_network.iloc[:, 4].values


# In[7]:


print(y)


# #### 3. Visualize the dataset

# ##### Visualizing dataset by drawing correlation map

# In[8]:


sns.heatmap(social_network.corr())


# #### 4. Split the dataset into Training and Testing Set

# In[27]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)


# #### 5. Feature Scaling

# In[28]:


from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)


# #### 6. Fit Logistic Regression to Training dataset

# In[37]:


from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state=0)
classifier.fit(X_train, y_train)


# #### 7. Predicting the test set results

# In[36]:


y_pred = classifier.predict(X_test)
print(y_pred)


# #### 8. Visualize the train set results

# In[31]:


from matplotlib.colors import ListedColormap
X_set, y_set = X_train, y_train
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                    np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))


# In[38]:


plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
            alpha = 0.75, cmap = ListedColormap(('blue', 'yellow')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set==j, 0], X_set[y_set == j, 1],
               c = ListedColormap(('red', 'green'))(i), label = j)
plt.title("Logistic Regression (Training Set)")
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()


# #### 8. Visualize the test set results

# In[39]:


X_set, y_set = X_test, y_test
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                    np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
            alpha = 0.75, cmap = ListedColormap(('blue', 'yellow')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set==j, 0], X_set[y_set == j, 1],
               c = ListedColormap(('red', 'green'))(i), label = j)
plt.title("Logistic Regression (Testing Set)")
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()


# #### 11. Evaluating the model

# ##### Confusion metrix to evaluate the model

# In[40]:


from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)


# In[ ]:





# In[ ]:




