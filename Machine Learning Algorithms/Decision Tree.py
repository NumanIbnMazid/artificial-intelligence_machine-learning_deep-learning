
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# get_ipython().run_line_magic('matplotlib', 'inline')


# #### 2. Import the dataset and Extract the Independent and Dependent variable

# In[3]:


kyphosis = pd.read_csv("kyphosis.csv")


# ##### Extracting Independent variable

# In[4]:


X = kyphosis.drop("Kyphosis", axis=1)


# ##### Extracting the Dependent variable

# In[5]:


y = kyphosis["Kyphosis"]


# #### 3. Visualise the data

# In[8]:


plt.figure(figsize=(25, 7))
sns.countplot(x="Age", hue="Kyphosis", data=kyphosis, palette='Set1')


# #### 4. Split the data into train and test set

# In[9]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=100)


# #### 5. Train a Decision Tree

# In[10]:


from sklearn.tree import DecisionTreeClassifier
dtree = DecisionTreeClassifier()


# In[12]:


dtree.fit(X_train, y_train)


# #### 6. Predict the model

# In[21]:


predictions = dtree.predict(X_test)
print(predictions)


# #### 7. Evaluate the model

# In[17]:


from sklearn.metrics import classification_report, confusion_matrix


# In[18]:


print(classification_report(y_test, predictions))


# In[20]:


print(confusion_matrix(y_test, predictions))


# In[ ]:




