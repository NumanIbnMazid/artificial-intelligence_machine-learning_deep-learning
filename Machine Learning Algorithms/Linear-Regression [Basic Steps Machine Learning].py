import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# get_ipython().run_line_magic('matplotlib', 'inline')


# #### 2. Load the Dataset

# In[2]:


salary_data = pd.read_csv('employee_salary.csv')


# In[3]:


salary_data.head()


# In[4]:


salary_data.drop(axis=1, labels=["Name", "Job_Title", "Hire_Date"], inplace=True)
salary_data.reindex(columns=["Experience", "Salary"])
salary_data.head()


# In[14]:


X = salary_data.iloc[:, :-1].values


# In[15]:


print(X)


# In[16]:


y = salary_data.iloc[:, 1].values


# In[17]:


print(y)


# #### 3. Visualise the data

# In[18]:


sns.barplot(x="Experience", y="Salary", data=salary_data)


# #### 4. Split the data into training and testing set

# In[23]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1/3, random_state=0)


# #### 5. Fit Simple Linear Regression to the training dataset

# In[24]:


from sklearn.linear_model import LinearRegression
lr = LinearRegression()
lr.fit(X_train, y_train)


# #### 6. Predict the test set results

# In[27]:


y_pred = lr.predict(X_test)
print(y_pred)


# #### 7. Visualise the train set results

# In[29]:


plt.scatter(X_train, y_train, color='blue')


# In[30]:


plt.plot(X_train, lr.predict(X_train), color="red")


# In[35]:


plt.title("Salary ~ Experience (Train Set)")
plt.xlabel("Experience")
plt.ylabel("Salary")
plt.show()


# #### 8. Visualise the test set results

# In[37]:


plt.scatter(X_test, y_test, color='blue')
plt.plot(X_train, lr.predict(X_train), color="red")
plt.title("Salary ~ Experience (Test Set)")
plt.xlabel("Experience")
plt.ylabel("Salary")
plt.show()


# #### 9. Calculate the residuals

# In[40]:


from sklearn import metrics
print("MAE:", metrics.mean_absolute_error(y_test, y_pred))
print("MSE:", metrics.mean_squared_error(y_test, y_pred))
print("RMSE:", np.sqrt(metrics.mean_absolute_error(y_test, y_pred)))


# In[ ]:




