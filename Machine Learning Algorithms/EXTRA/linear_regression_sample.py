import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os

# Starts Display Customization
desired_width = 320
pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 50)
# Ends Display Customization

salary_data = pd.read_csv('salary_data.csv')
print(salary_data.head(100))
print(salary_data.info())