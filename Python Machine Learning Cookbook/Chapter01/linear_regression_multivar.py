import pandas as pd
import seaborn as sns
import os
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import _pickle as pickle
import sklearn.metrics as sm
import sys
import numpy as np

"""
One of the main problems of linear regression is that it's sensitive to outliers. Linear regression
uses ordinary least squares, which tries to minimize the squares of errors. The outliers tend to cause problems because they contribute a lot to the overall error. This tends to disrupt the entire model. To avoid this, we use regularization where a penalty is imposed on the size of the  coefficients. This method is called Ridge Regression.
"""

BASE_DIR = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))

filename = f"{BASE_DIR}/data_multivar.txt"

X = []
y = []

with open(filename, 'r') as f:
    for i, line in enumerate(f.readlines()):
        data = [float(i) for i in line.split(',')]
        xt, yt = data[:-1], data[-1]
        """ Start of demonstrating code occurances """
        if i == 0:
            print("First Data: ", data)
            # [0.39, 2.78, 7.11, -8.07]
            print("First xt: ", xt)
            # [0.39, 2.78, 7.11]
            print("First yt: ", yt)
            # -8.07
        """ End of demonstrating code occurances """
        X.append(xt)
        y.append(yt)

print("X: \n", X)
print("y: \n", y)

# Train/test split
"""80% of the data for the training dataset and the remaining 20% for the testing dataset."""
num_training = int(0.8 * len(X))
num_test = len(X) - num_training
print("\n Num Training: \n", num_training)
# 400
print("\n Num Test: \n", num_test)
# 100

# Training data
X_train = np.array(X[:num_training])
y_train = np.array(y[:num_training])
print("\n X_train: \n", X_train)
print("\n y_train: \n", y_train)
print("\n Length of X_train: \n", len(X_train))
print("\n Length of y_train: \n", len(y_train))

# Test Data
X_test = np.array(X[num_training:])
y_test = np.array(y[num_training:])
print("\n X_test: \n", X_test)
print("\n y_test: \n", y_test)
print("\n Length of X_test: \n", len(X_test))
print("\n Length of y_test: \n", len(y_test))

# Create Linear Regression Object
linear_regressor = linear_model.LinearRegression()
ridge_regressor = linear_model.Ridge(alpha=0.01, fit_intercept=True, max_iter=10000)

"""
The alpha parameter controls the complexity. As alpha gets closer to 0, the ridge regressor tends to become more like a linear regressor with ordinary least squares. So, if you want to make it robust to alpha. We considered a value of 0.01, which is moderate.
"""

# Train the model using the training sets

linear_regressor.fit(X_train, y_train)
ridge_regressor.fit(X_train, y_train)

# Predict the output
y_test_pred = linear_regressor.predict(X_test)
y_test_pred_ridge = ridge_regressor.predict(X_test)

# Measure performance

print("Linear Regression")
print("Mean Absolute Error: ", round(
    sm.mean_absolute_error(y_test, y_test_pred), 2))
print("Mean Squared Error: ", round(
    sm.mean_squared_error(y_test, y_test_pred), 2))
print("Median Absolute Error: ", round(
    sm.median_absolute_error(y_test, y_test_pred), 2))
print("Explained Variance Score: ", round(
    sm.explained_variance_score(y_test, y_test_pred), 2))
print("R2 Score: ", round(sm.r2_score(y_test, y_test_pred), 2))

print("Ridge Regression")
print("Mean Absolute Error: ", round(
    sm.mean_absolute_error(y_test, y_test_pred_ridge), 2))
print("Mean Squared Error: ", round(
    sm.mean_squared_error(y_test, y_test_pred_ridge), 2))
print("Median Absolute Error: ", round(
    sm.median_absolute_error(y_test, y_test_pred_ridge), 2))
print("Explained Variance Score: ", round(
    sm.explained_variance_score(y_test, y_test_pred_ridge), 2))
print("R2 Score: ", round(sm.r2_score(y_test, y_test_pred_ridge), 2))


# Building a polynomial regressor
"""
One of the main constraints of a linear regression model is the fact that it tries to fit a linear function to the input data. The polynomial regression model overcomes this issue by allowing the function to be a polynomial, thereby increasing the accuracy of the model.  The curviness of this model is controlled by the degree of the polynomial. As the curviness of the model increases, it gets more accurate. However, curviness adds complexity to the model as well, hence, making it slower. This is a trade off where you have to decide between how accurate you want your model to be given the computational constraints.
"""

polynomial = PolynomialFeatures(degree=10)
"""
We initialized a polynomial of the degree 3 in the previous line (Degree 10 given after updated). Now we have to represent the datapoints in terms of the coefficients of the polynomial:
"""
X_train_transformed = polynomial.fit_transform(X_train)
"""Here, X_train_transformed represents the same input in the polynomial form."""

"""
Let's consider the first datapoint in our file and check whether it can predict the right output:
"""
datapoint = [[0.39, 2.78, 7.11]]
poly_datapoint = polynomial.fit_transform(datapoint)

poly_linear_model = linear_model.LinearRegression()
poly_linear_model.fit(X_train_transformed, y_train)

print("\n Linear Regression: \n", linear_regressor.predict(datapoint))
print("\n Polynomial Regression: \n", poly_linear_model.predict(poly_datapoint)[0])

"""
The values in the variable datapoint are the values in the first line in the input data file. We are still fitting a linear regression model here. The only difference is in the way in which we represent the data. If you run this code, you will see the following output:
Linear regression: -11.0587294983
Polynomial regression: -10.9480782122
As you can see, this is close to the output value. If we want it to get closer, we need to increase the degree of the polynomial.
"""


# Stochastic Gradient Descent regressor
# sgd_regressor = linear_model.SGDRegressor(loss='huber', n_iter=50)
sgd_regressor = linear_model.SGDRegressor(loss='huber')
sgd_regressor.fit(X_train, y_train)
print("\n SGD Regressor:\n", sgd_regressor.predict(datapoint))
