import os
from sklearn import linear_model
import matplotlib.pyplot as plt
import _pickle as pickle
import sklearn.metrics as sm
import sys
import numpy as np

"""
Regression is the process of estimating the relationship between input data and the continuous-valued output data. This data is usually in the form of real numbers, and our goal is to estimate the underlying function that governs the mapping from the input to the output.
Linear regression refers to estimating the underlying function using a linear combination of
input variables.
The goal of linear regression is to extract the underlying linear model that relates the input variable to the output variable. This aims to minimize the sum of squares of differences between the actual output and the predicted output using a linear function. This method is called Ordinary least squares.
"""

BASE_DIR = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))
filename = f"{BASE_DIR}/data_singlevar.txt"
X = []
y = []

with open(filename, 'r') as f:
    for line in f.readlines():
        xt, yt = [float(i) for i in line.split(',')]
        X.append(xt)
        y.append(yt)

"""X refers to data and y refers to labels"""

print("\n X: \n", X)
print("\n y: \n", y)

# Train/test split

"""
The training dataset will be used to build the model, and the testing dataset will be used to see how this trained model performs on unknown data.
"""

"""80% of the data for the training dataset and the remaining 20% for the testing dataset."""

num_training = int(0.8 * len(X))
num_test = len(X) - num_training

print("\n Num Training: \n", num_training)
# 40
print("\n Num Test: \n", num_test)
# 10
print(len(X))
# 50
print(len(y))
# 50

# Training Data
X_train = np.array(X[:num_training]).reshape((num_training, 1))
y_train = np.array(y[:num_training])
print("\n X_train: \n", X_train)
print("\n y_train: \n", y_train)

# Testing Data
X_test = np.array(X[num_training:]).reshape(num_test, 1)
y_test = np.array(y[num_training:])

print("\n X_test: \n", X_test)
print("\n y_test: \n", y_test)

# Create linear regression object

linear_regressor = linear_model.LinearRegression()

# Train the model using the training sets
linear_regressor.fit(X_train, y_train)
"""The fit method takes the input data and trains the model."""

# Predict the output (Training Data)
y_train_pred = linear_regressor.predict(X_train)

# Visualize
plt.scatter(X_train, y_train, color='green')
plt.plot(X_train, y_train_pred, color='black', linewidth=4)
plt.title("Training Data")
plt.xticks(())
plt.yticks(())
plt.show()

# Predict the output (Testing Data)
y_test_pred = linear_regressor.predict(X_test)

# Visualize
plt.scatter(X_test, y_test, color='green')
plt.plot(X_test, y_test_pred, color='black', linewidth=4)
plt.title("Test Data")
plt.show()

"""
In the preceding code, we used the trained model to predict the output for our training data. This wouldn't tell us how the model performs on unknown data because we are running it on training data itself. This just gives us an idea of how the model fits on training data. 
"""

# Computing regression accuracy
# Measure performance
"""
In this context, an error is defined as the difference between the actual value and the value that is predicted by the regressor.
"""
"""
Let's quickly understand what metrics can be used to measure the quality of a regressor. A regressor can be evaluated using many different metrics, such as the following:
- Mean absolute error: This is the average of absolute errors of all the datapoints in the given dataset.
- Mean squared error: This is the average of the squares of the errors of all the datapoints in the given dataset. It is one of the most popular metrics out there!
- Median absolute error: This is the median of all the errors in the given dataset. The main advantage of this metric is that it's robust to outliers. A single bad point in the test dataset wouldn't skew the entire error metric, as opposed to a mean error metric.
- Explained variance score: This score measures how well our model can account for the variation in our dataset. A score of 1.0 indicates that our model is perfect.
- R2 score: This is pronounced as R-squared, and this score refers to the coefficient of determination. This tells us how well the unknown samples will be predicted by our model. The best possible score is 1.0, and the values can be negative as well.
"""

print("Mean absolute error =", round(
    sm.mean_absolute_error(y_test, y_test_pred), 2)
)
print("Mean squared error =", round(
    sm.mean_squared_error(y_test, y_test_pred), 2)
)
print("Median absolute error =", round(
    sm.median_absolute_error(y_test, y_test_pred), 2)
)
print("Explain variance score =", round(
    sm.explained_variance_score(y_test, y_test_pred), 2)
)
print("R2 score =", round(
    sm.r2_score(y_test, y_test_pred), 2)
)

# Achieving Model persistence

"""
When we train a model, it would be nice if we could save it as a file so that it can be used later by simply loading it again.
"""

output_model_file = 'saved_model.pkl'

with open(output_model_file, 'wb') as f:
    pickle.dump(linear_regressor, f)
""" wb for write binary file """
"""
The regressor object will be saved in the saved_model.pkl file. Let's look at how to load it and use it, as follows:
"""

with open(output_model_file, 'rb') as f:
    model_linregr = pickle.load(f)

""" rb for read binary file """

y_test_pred_new = model_linregr.predict(X_test)

print("\n New mean absolute error =", round(
    sm.mean_absolute_error(y_test, y_test_pred_new), 2)
)

"""
Here, we just loaded the regressor from the file into the model_linregr variable. You can compare the preceding result with the earlier result to confirm that it's the same.
"""
