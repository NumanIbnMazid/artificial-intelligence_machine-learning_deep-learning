import numpy as np
from sklearn import preprocessing

"""
Label encoding refers to transforming the word labels into numerical form so that the algorithms can understand how to operate on them. 
"""

label_encoder = preprocessing.LabelEncoder()
input_classes = ['audi', 'ford', 'audi', 'toyota', 'ford', 'bmw']
label_encoder.fit(input_classes)

print('\n Label Encoder Classes: ', label_encoder.classes_)
# ['audi' 'bmw' 'ford' 'toyota']

print("\n Class Mapping:")
for i, item in enumerate(label_encoder.classes_):
    print(item, '-->', i)
# audi --> 0
# bmw --> 1
# ford --> 2
# toyota --> 3

"""
The words have been transformed into 0-indexed numbers. Now, when you encounter a set of labels, you can simply transform them, as follows:
"""

labels = ['toyota', 'ford', 'audi']
encoded_labels = label_encoder.transform(labels)
print("\n Labels: ", labels)
# ['toyota', 'ford', 'audi']
print("\n Encoded Labels: ", list(encoded_labels))
# [3, 2, 0]

"""
This is way easier than manually maintaining mapping between words and numbers. You can check the correctness by transforming numbers back to word labels:
"""

encoded_labels = [2, 1, 0, 3, 1]
decoded_labels = label_encoder.inverse_transform(encoded_labels)
print("\n Encoded Labels: ", encoded_labels)
# [2, 1, 0, 3, 1]
print("\n Decoded Labels: ", decoded_labels)
# ['ford' 'bmw' 'audi' 'toyota' 'bmw']

