import numpy as np
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn import datasets
from sklearn.metrics import mean_squared_error, explained_variance_score
from sklearn.utils import shuffle
import matplotlib.pyplot as plt


def plot_feature_importances(feature_importances, title, feature_names):
    print("\n feature_importances: \n", feature_importances)
    print("\n title: \n", title)
    print("\n feature_names: \n", feature_names)
    """ Normalize the importance values """

    feature_importances = 100.0 * (feature_importances / max(feature_importances))

    """
    We just take the values from the feature_importances_ method and scale it so that it ranges between 0 and 100.
    """

    print("\n normalized feature_importances: \n", feature_importances)

    """
    Sort the index values and flip them so that they are arranged in decreasing order of importance
    """
    
    index_sorted = np.flipud(np.argsort(feature_importances))

    print("\n index_sorted: \n", index_sorted)

    """ Center the location of the labels on the X-axis (for display purposes only)"""
    # pos = np.arange(index_sorted.shape[0] + 0.5)
    pos = np.arange(index_sorted.shape[0])

    print("\n pos: \n", pos)
    
    # Plot the bar graph
    plt.figure()
    plt.bar(pos, feature_importances[index_sorted], align='center')
    plt.xticks(pos, feature_names[index_sorted])
    plt.ylabel("Relative Importance")
    plt.title(title)
    plt.show()


if __name__ == '__main__':

    # Load Housing Data
    housing_data = datasets.load_boston()

    print("\n Input Data: \n", housing_data.data)
    print("\n Corresponding Price: \n", housing_data.target)

    print("Feature Names: ", housing_data.feature_names)
    # ['CRIM' 'ZN' 'INDUS' 'CHAS' 'NOX' 'RM' 'AGE' 'DIS' 'RAD' 'TAX' 'PTRATIO'
    #  'B' 'LSTAT']

    """
    Let's separate this into input and output. To make this independent of the ordering of the data, let's shuffle it as well:
    """

    # Shuffle the data
    X, y = shuffle(housing_data.data, housing_data.target, random_state=7)

    """
    The random_state parameter controls how we shuffle the data so that we can have reproducible results.
    """

    """
    Let's divide the data into training and testing. We'll allocate 80% for training and 20% for testing:
    """
    print("Length of X: ", len(X))
    # 506
    print("Length of y: ", len(y))
    # 506

    # Split Data 80/20
    num_training = int(0.8 * len(X))
    print("Number of training: ", num_training)
    # 404
    num_testing = len(X) - num_training
    print("Number of testing: ", num_testing)
    # 102

    X_train, y_train = X[:num_training], y[:num_training]
    X_test, y_test = X[num_training:], y[num_training:]

    """
    We are now ready to fit a decision tree regression model. Let's pick a tree with a maximum depth of 4, which means that we are not letting the tree become arbitrarily deep:
    """

    dt_regressor = DecisionTreeRegressor(max_depth=4)
    dt_regressor.fit(X_train, y_train)

    """
    Let's also fit decision tree regression model with AdaBoost:
    """

    ab_regressor = AdaBoostRegressor(DecisionTreeRegressor(max_depth=4), n_estimators=400, random_state=7)
    ab_regressor.fit(X_train, y_train)

    """
    This will help us compare the results and see how AdaBoost really boosts the performance of a decision tree regressor.
    """

    # Predict the  output

    y_pred_dt = dt_regressor.predict(X_test)
    y_pred_ab = ab_regressor.predict(X_test)

    # Performance Evaluation
    print("Performance of Decission Tree Regression: ")
    mse = mean_squared_error(y_test, y_pred_dt)
    evs = explained_variance_score(y_test, y_pred_dt)
    print("Mean Squared Error: ", round(mse, 2))
    # 14.79 -> Error rate high
    print("Explained Variance Score: ", round(evs, 2))
    # 0.82 -> Less Closer to 1

    print("Performance of Ada Boosting Regression: ")
    mse = mean_squared_error(y_test, y_pred_ab)
    evs = explained_variance_score(y_test, y_pred_ab)
    print("Mean Squared Error: ", round(mse, 2))
    # 7.66 -> Error rate low
    print("Explained Variance Score: ", round(evs, 2))
    # 0.91 -> Closer to 1

    """
    Are all the features equally important? In this case, we used 13 input features, and they all contributed to the model. However, an important question here is, "How do we know which features are more important?" Obviously, all the features don't contribute equally to the output. In case we want to discard some of them later, we need to know which features are less important. We have this functionality available in scikit-learn.
    """

    # Plot relative feature importances
    plot_feature_importances(dt_regressor.feature_importances_,
                            'Decision Tree regressor', housing_data.feature_names)
    plot_feature_importances(ab_regressor.feature_importances_,
                            'AdaBoost regressor', housing_data.feature_names)


# Analyze
"""
So, the decision tree regressor says that the most important feature is RM
"""
"""
According to AdaBoost, the most important feature is LSTAT. In reality, if you build various regressors on this data, you will see that the most important feature is in fact LSTAT. This shows the advantage of using AdaBoost with a decision tree-based regressor.
"""