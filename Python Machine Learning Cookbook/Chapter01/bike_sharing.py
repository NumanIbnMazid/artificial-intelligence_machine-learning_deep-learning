import csv
from sklearn.ensemble import RandomForestRegressor
from housing import plot_feature_importances
import numpy as np
import sys
import os
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error, explained_variance_score
import matplotlib.pyplot as plt


BASE_DIR = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))

file = f"{BASE_DIR}/bike_day.csv"

def load_dataset(filename):
    file_reader = csv.reader(open(filename, "rt"), delimiter=',')
    X, y = [], []
    for row in file_reader:
        X.append(row[2:13])
        y.append(row[-1])

    """
    The first two columns correspond to the serial number and the actual date, so we won't use them for our analysis. The last three columns correspond to different types of outputs. The last column is just the sum of the values in the fourteenth and fifteenth columns, so we can leave these two out when we build our model.
    """

    print("\n X[0] : \n", X[0])

    # Extract feature names
    feature_names = np.array(X[0])

    # Remove the first row because they are feature names
    return np.array(X[1:]).astype(np.float32), np.array(y[1:]).astype(np.float32), feature_names


X, y, feature_names = load_dataset(file)

print("\n X \n", X)
print("\n y \n", y)
print("\n feature_names \n", feature_names)

X, y = shuffle(X, y, random_state=7)

num_training = int(0.9 * len(X))
num_testing = len(X) - num_training

print(" len(X): ", len(X))
print("num_training: ", num_training)
print("num_testing: ", num_testing)

X_train, y_train = X[:num_training], y[:num_training]
X_test, y_test = X[num_training:], y[num_training:]

# Train with RandomForestRegressor
rf_regressor = RandomForestRegressor(n_estimators=1000, max_depth=10, min_samples_split=1.0)
"""
Here, n_estimators refers to the number of estimators, which is the number of decision trees that we want to use in our random forest. The max_depth parameter refers to the maximum depth of each tree, and the min_samples_split parameter refers to the number of data samples that are needed to split a node in the tree.
"""

rf_regressor.fit(X_train, y_train)

# Perdict the output
y_pred = rf_regressor.predict(X_test)

# Measure performance
mse = mean_squared_error(y_test, y_pred)
evs = explained_variance_score(y_test, y_pred)

print("\n Mean Squared Error: ", mse)
print("\n Explained Variance Score: ", evs)


plot_feature_importances(rf_regressor.feature_importances_, 'Random Forest Regressor', feature_names)


