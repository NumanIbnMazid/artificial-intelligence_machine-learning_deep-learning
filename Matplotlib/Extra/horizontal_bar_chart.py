from matplotlib import pyplot as plt
import numpy as np
import csv
from collections import Counter
import pandas as pd

plt.style.use('fivethirtyeight')


# with open('data.csv') as csv_file:
#     csv_reader = csv.DictReader(csv_file)
#     language_counter = Counter()
#     # row = next(csv_reader)  # first line
#     # print(row)
#     # language_worked = row['LanguagesWorkedWith'].split(';')
#     # print(language_worked)
#     for row in csv_reader:
#         language_counter.update(row['LanguagesWorkedWith'].split(';'))

data = pd.read_csv('data.csv')
ids = data['Responder_id']
lang_responses = data['LanguagesWorkedWith']
language_counter = Counter()
for response in lang_responses:
    language_counter.update(response.split(';'))

# print(language_counter)
# print(language_counter.most_common(15))

languages = []
popularity = []

for item in language_counter.most_common(15):
    languages.append(item[0])  # as it is tuple
    popularity.append(item[1])

# print(languages)
# print(popularity)

languages.reverse()  # to show most used first
popularity.reverse()  # to show most used first

plt.barh(languages, popularity)

plt.ylabel("Programming Languages")
plt.xlabel("Number of People Use")
plt.title("Most Popular Languages")
# plt.legend()
plt.tight_layout()
plt.show()