from matplotlib import pyplot as plt
plt.style.use('fivethirtyeight')

# slices = [60, 40, 30, 20]
# labels = ['Sixty', 'Forty', 'Thirty', 'Twenty']
# colors = ['#008fd5', '#fc4f30', '#e5ae37', '#6d904f']

# Language Popularity
# slices = [59219, 55466, 47544, 36443, 35917, 31991, 27097, 23030, 20524, 18523, 18017, 7920, 7331, 7201, 5833]
# labels = ['JavaScript', 'HTML/CSS', 'SQL', 'Python', 'Java', 'Bash/Shell/PowerShell', 'C#', 'PHP', 'C++', 'TypeScript', 'C', 'Other(s):', 'Ruby', 'Go', 'Assembly']

slices = [59219, 55466, 47544, 36443, 35917]
labels = ['JavaScript', 'HTML/CSS', 'SQL', 'Python', 'Java']
explode = [0, 0, 0, 0.1, 0]  # Python will be exploded based on the list index

# plt.pie(slices, labels=labels, colors=colors, wedgeprops={'edgecolor': 'black'})
plt.pie(slices, labels=labels, explode=explode,
        shadow=True, startangle=90, autopct='%1.1f%%',
        wedgeprops={'edgecolor': 'black'})

plt.title("My Pie Chart")
plt.tight_layout()
plt.show()
