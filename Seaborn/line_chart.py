import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

sales = pd.read_excel('sales.xlsx')

# sns.relplot('Order Date', 'Sales', data=sales, kind='line')
sales['year'] = sales['Order Date'].dt.year
# sns.relplot('year', 'Sales', data=sales, kind='line')
# sns.relplot('year', 'Sales', data=sales, kind='line', ci='sd')  # sd = Standard Deviation
# sns.relplot('year', 'Sales', data=sales, kind='line', ci=None)  # ci -> confidence interval
# sns.relplot('year', 'Sales', data=sales, kind='line', estimator=None)
# estimator=None -> Shows actual Data
# sns.relplot('year', 'Sales', data=sales, kind='line', estimator=sum)
# sns.relplot('year', 'Sales', data=sales, kind='line', hue='Ship Mode')
# sns.relplot('year', 'Sales', data=sales, kind='line', hue='Ship Mode', style='Product Category', ci=None)
# sns.relplot('year', 'Sales', data=sales, kind='line', hue='Ship Mode', col='Product Category', ci=None)
sns.relplot('year', 'Sales', data=sales, kind='line', hue='Ship Mode', col='Region', col_wrap=4, ci=None)
# col creates different charts based on the categories, col_wrap=4 -> Break columns by 4




plt.show()