import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

sales = pd.read_excel('sales.xlsx')

# sns.catplot("Ship Mode", "Sales", data=sales)
# sns.catplot("Ship Mode", "Sales", data=sales, jitter=False)
# jitter=False -> narrow down the width of data and shows in line
# sns.catplot("Ship Mode", "Sales", data=sales.query("Profit > 1000"), jitter=False)
sns.catplot("Sales", "Ship Mode", data=sales.query("Profit > 1000"), kind='swarm')
# Horizontal by reversing item

plt.show()