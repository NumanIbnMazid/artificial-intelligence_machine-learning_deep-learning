import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

sales = pd.read_excel('sales.xlsx')

sales['year'] = sales['Order Date'].dt.year

# sns.relplot('Sales', 'Profit', data=sales, col='Customer Segment', col_wrap=2, hue='Order Priority')
sns.relplot('Sales', 'Profit', data=sales, col='Customer Segment', row='Product Category', hue='Order Priority')

plt.show()