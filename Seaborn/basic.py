import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

sales = pd.read_excel('sales.xlsx')

# print(sales.columns)

# sns.relplot('Sales', 'Profit', data=sales)

# sns.relplot('Sales', 'Profit', data=sales, hue='Order Priority')  # hue -> shows categorical data colred

# sns.relplot('Sales', 'Profit', data=sales, hue='Order Priority', style='Ship Mode')
# style -> styles categorical data like addition symbol, circle, cross etc.

sns.relplot('Sales', 'Profit', data=sales, hue='Order Priority', style='Ship Mode', size='Discount', sizes=(25, 200))
# size -> resize shapes based on their value, sizes -> replaces the default size

plt.show()